package homeworks.x0x0;

public class ConsoleAnimation {

    private String lastLine = "";

    public void print(String line) {
        if (lastLine.length() > line.length()) {
            String temp = "";
            for (int i = 0; i < lastLine.length(); i++) {
                temp += " ";
            }
            if (temp.length() > 1)
                System.out.print("\r" + temp);
        }
        System.out.print("\r" + line);
        lastLine = line;
    }

    private byte anim;

    public void animate(String line) {
        switch (anim) {
            case 1:
                print("Loading \\ " + line);
                break;
            case 2:
                print("Loading | " + line);
                break;
            case 3:
                print("Loading / " + line);
                break;
            default:
                anim = 0;
                print("Loading - " + line);
        }
        anim++;
    }

    public void main(String[] args) throws InterruptedException {
        loading();
    }
    public static void loading() throws InterruptedException {
        ConsoleAnimation consoleHelper = new ConsoleAnimation();
        for (int i = 0; i < 14; i++) {
            consoleHelper.animate("");
            Thread.sleep(210);
        }
    }
}