package homeworks.x0x0;

import java.util.Scanner;

import static java.lang.Thread.*;

public class krestiki {
    private static int moves = 0;

    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("                  'TicTacToe'");
        sleep(1000);
        System.out.print("             ");
        String str = "Solevarnya Inc. 2020.";
        char[] sol = str.toCharArray();
        for (char c : sol) {
            System.out.print(c);
            sleep(210);
        }
        System.out.println();
        sleep(1000);
        System.out.print("              All rights reserved.");
        sleep(750);
        System.out.println();
        System.out.print("                      .");
        sleep(250);
        System.out.print(".");
        sleep(250);
        System.out.print(".");
        sleep(500);
        System.out.println();
        sleep(500);
        boolean isEnded = false;
        String[][] board = new String[3][3];
        int[][] intboard = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = " ";
                intboard[i][j] = -1;
            }
        }
        System.out.println("                      \n" +
                "              |  1  |  2  |  3  |\n" +
                "           ---|-----|-----|-----|\n" +
                "            a | a1  | a2  | a3  |\n" +
                "           ---|-----|-----|-----|\n" +
                "            b | b1  | b2  | b3  |\n" +
                "           ---|-----|-----|-----|\n" +
                "            c | c1  | c2  | c3  |\n" +
                "           ---|-----|-----|-----|\n" +
                "                [Navigation]");
        boolean inputDone = false;
        while (!inputDone) {
            System.out.println("Enter:");
            System.out.println("\"/start\" to begin;");
            System.out.println("\"/rules\" for rules;");
            System.out.println("\"/end\" to exit;");
            String input = sc.nextLine();
            //sleep(500);
            switch (input) {
                case ("/end"):
                    isEnded = true;
                    inputDone = true;
                    System.out.println("Understandable, have a nice day!");
                    break;
                case ("/rules"):
                    rules();
                    sleep(2500);
                    break;
                case ("/start"):
                    ConsoleAnimation.loading();
                    System.out.println();
                    inputDone = true;
                    print(board);
                    break;
                default:
                    System.out.println("Please, enter something sensible.");
                    sleep(500);
            }
        }
        // чтоб идея не ругалась
        String p1 = "p1";
        String p2 = "p2";
        if (!isEnded) {
            System.out.println("Player 1 (0), introduce yourself:");
            p1 = sc.nextLine();
            System.out.println("Player 2 (X), say your name..");
            p2 = sc.nextLine();
        }
        int c1 = 0;
        int c2 = 0;
        while (!isEnded) {
            String change;
            int intchange;
            if (moves < 9) {
                if ((moves % 2 == 0)) {
                    change = "O";
                    intchange = 0;
                    System.out.println(p1 + " (0) , your move?");
                } else {
                    change = "X";
                    intchange = 1;
                    System.out.println(p2 + " (X) , your move?");
                }
                String move = sc.nextLine();
                if (move.equals("/end")) {
                    System.out.println("A, tebe pora, nu ladno...");
                    isEnded = true;
                    continue;
                }
                boolean moveIsValid = true;
                int line = 0;
                int row = 0;
                if (move.length() == 2) {
                    switch (move.charAt(0)) {
                        case ('A'):
                        case ('a'):
                            line = 0;
                            break;
                        case ('B'):
                        case ('b'):
                            line = 1;
                            break;
                        case ('C'):
                        case ('c'):
                            line = 2;
                            break;
                        default:
                            moveIsValid = false;
                            break;
                    }
                    row = move.charAt(1);
                    row = Character.getNumericValue(row) - 1;
                    if ((row < 0) | (row > 2)) {
                        moveIsValid = false;
                    }
                } else {
                    moveIsValid = false;
                }
                if ((moveIsValid) && (intboard[line][row] == -1)) {
                    board[line][row] = change;
                    intboard[line][row] = intchange;
                    print(board);
                } else {
                    System.out.print("bruh...");
                    sleep(1000);
                    System.out.println("let's try again:");
                    sleep(1000);
                    moves--;
                }
            } else {
                isEnded = true;
            }
            String winner = "Nobody";
            int intwinner = -1;
            if (moves != 9) {
                // Horizontal check
                for (int i = 0; i < 3; i++) {
                    if ((intboard[i][0] == intboard[i][1]) && (intboard[i][1] == intboard[i][2])) {
                        if (intboard[i][0] != -1) {
                            intwinner = intboard[i][0];
                            isEnded = true;
                        }
                    }
                }
                // Vertical check
                for (int i = 0; i < 3; i++) {
                    if ((intboard[0][i] == intboard[1][i]) && (intboard[1][i] == intboard[2][i])) {
                        if (intboard[0][i] != -1) {
                            intwinner = intboard[0][i];
                            isEnded = true;
                        }
                    }
                }
                // Diagonal check
                if ((intboard[0][0] == intboard[1][1]) && (intboard[1][1] == intboard[2][2])) {
                    if (intboard[0][0] != -1) {
                        intwinner = intboard[0][0];
                        isEnded = true;
                    }
                }
                if ((intboard[2][0] == intboard[1][1]) && (intboard[1][1] == intboard[0][2])) {
                    if (intboard[2][0] != -1) {
                        intwinner = intboard[2][0];
                        isEnded = true;
                    }
                }
            } else {
                isEnded = true;
            }
            if (isEnded) {
                if (intwinner == 0) {
                    winner = p1;
                    c1++;
                }
                if (intwinner == 1) {
                    winner = p2;
                    c2++;
                }
                System.out.println();
                System.out.println("                  " + winner + " wins!");
                System.out.println();
                System.out.println("Score: " + p1 + " (0) " + c1 + "-" + c2 + " (X) " + p2 + " ;");
                System.out.println();
                boolean startover = false;
                System.out.println("            Another round? ( Y / N )");
                String choice = sc.nextLine();
                if ("y".equals(choice.toLowerCase())) {
                    isEnded = false;
                    startover = true;
                } else {
                    System.out.println("Total score : " + p1 + " " + c1 + "-" + c2 + " " + p2);
                    if(c1==c2){
                        System.out.println("Draw!");
                    } else {
                        if (c1 > c2) {
                            System.out.println(p1 + " won!");
                        } else {
                            System.out.println(p2 + " won!");
                        }
                    }
                }
                if (startover) {
                    System.out.println("            Switch sides? ( Y / N )");
                    choice = sc.nextLine();
                    if ("y".equals(choice.toLowerCase())) {
                        int temp = c1;
                        c1 = c2;
                        c2 = temp;
                        String strtemp = p1;
                        p1 = p2;
                        p2 = strtemp;
                    }
                    System.out.print("Cleaning up the board");
                    sleep(250);
                    System.out.print(".");
                    sleep(250);
                    System.out.print(".");
                    sleep(250);
                    System.out.print(".");
                    sleep(1000);
                    System.out.println();
                    System.out.println();
                    for (int i = 0; i < 3; i++) {
                        for (int j = 0; j < 3; j++) {
                            board[i][j] = " ";
                            intboard[i][j] = -1;
                        }
                    }
                    moves = -1;
                    print(board);
                }
            }
            moves++;
        }
    }

    private static void print(String[][] board) {
            System.out.println("              |  1  |  2  |  3  |\n" +
                    "           ---|-----|-----|-----|\n" +
                    "            a |  " + board[0][0] + "  |  " + board[0][1] + "  |  " + board[0][2] + "  |\n" +
                    "           ---|-----|-----|-----|\n" +
                    "            b |  " + board[1][0] + "  |  " + board[1][1] + "  |  " + board[1][2] + "  |\n" +
                    "           ---|-----|-----|-----|\n" +
                    "            c |  " + board[2][0] + "  |  " + board[2][1] + "  |  " + board[2][2] + "  |\n" +
                    "           ---|-----|-----|-----|\n" +
                    "                                 ");
    }

    private static void rules() {
        System.out.println("@,,::::::::::::::::::;;;;;;;;;;:::::;;;;;::;;;;;#\n" +
                "@,,,,:::::,::::::::::;;''+++++;::::::::::;;;::;;#\n" +
                "@,,,,::::::::::,::::;''#######';;;;::::::;;;::;;#\n" +
                "@,,,,:::::::::::::;;+++###++++'::;;::::::;;;;;;;#\n" +
                "@,,,,,::::::::::::::+++''';;;;';;;;::::::;;;;;::#\n" +
                "@,,,,:::,,:::,,:::::+++''';;;;;::::;;;:::;;;::::#\n" +
                "@::::::::::::,,,::,,+++''''';;;:::::;;;;;::;::::#\n" +
                "@::::::::::;;:::::::'''''''''';:::::::;;:;;;::::#\n" +
                "@:::::::::;:::::::::;''''''''';:::::;;;;:;;;;;::#\n" +
                "@::::::::::::;;:::::;''''''''';;;::;;;::;::;;;;;#\n" +
                "@:::::::::;;;;;;::::;''+++''''';;;;;::::;;;;;;::#\n" +
                "@::::;;;;;:::;;;::::'''''+''''''';;::::::;;;;;::#\n" +
                "@;;;;;;;:::::;;;;;++#''++'''+++####+;;:::;;;;;::#\n" +
                "@;;:::;;:::;;;;+#####++''''''''#######++;;;;;;;;#\n" +
                "@:::::;;;;:''##########;;;'';;'#########;;;;;;;;#\n" +
                "@;;::;;;;;'############''''''''#########;;;;;;;;#\n" +
                "@;;;;;;;;;+##############'''''+#########;;;;;;;;#\n" +
                "@;;;;;;;;;###############'''++##########;;;;;;;;#\n" +
                "@::;;;;;;;#####@#########+++++##########';;;;;;;#\n" +
                "@::;;;;;;;###@@@#########+++++##########';;;;;;;#\n" +
                "@::;;;;;''###@@@@@@@######+++++''''#####+;;;;;;;#\n" +
                "@;;;;;;;++###@@@@@@@@#####++##+''''#####+;;;;;;;#\n" +
                "@;;;;;;;#####@@@@@@@@####@####+''''######;;;;;;;#\n" +
                "@;;;;;;;###@@@@@@@@@@#####++#####''######;;;;;;;#\n" +
                "@;;;;;;;#####@@@@@@@@@@##@#####@@#######+;;;;;;;#\n" +
                "@;;;;;;;###@@++'++@@@@@###++###@@@@#####;;;;;;;;#\n" +
                "@;;;;;;;#####'''''@@@@@@@#++#####@@#++;;;;;;;;;;#\n" +
                "@;;;;;;;++###++'''##@@@@@@#####@@@@;;;;;;;;;;;;;#\n" +
                "@;;;;;;;''#####+++@@@@@@@@#########';;;;;;;;;;;;#");
        System.out.println("♫ \uD835\uDCB4\uD835\uDC5C\uD835\uDCCA \uD835\uDCC0\uD835\uDCC3\uD835\uDC5C\uD835\uDCCC \uD835\uDCC9\uD835\uDCBD\uD835\uDC52 \uD835\uDCC7\uD835\uDCCA\uD835\uDCC1\uD835\uDC52\uD835\uDCC8, \uD835\uDCB6\uD835\uDCC3\uD835\uDCB9 \uD835\uDCC8\uD835\uDC5C \uD835\uDCB9\uD835\uDC5C \uD835\uDC3C... ♫");
    }
}