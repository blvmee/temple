package homeworks.Files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class FileMaster {

    public void makeFile(String filePath) throws IOException {
        File file = new File(Paths.get(filePath).toUri());
        if (file.exists()) {
            if (!file.isDirectory()) {
                File newFolder = new File(filePath.substring(0, filePath.lastIndexOf(".")));
                newFolder.mkdirs();
            }
        } else {
            file.createNewFile();
        }
    }

    public void printTree(String filePath, int maxDepth) {
        maxDepth += 1;
        File root = new File(Paths.get(filePath).toUri());
        if (root.isDirectory()) printFolderTree(root, 0, maxDepth);
    }

    private void printFolderTree(File file, int currentDepth, int maxDepth) {
        if (currentDepth == maxDepth) return;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < currentDepth; i++) {
            sb.append('\t');
        }
        sb.append(file.getName());
        System.out.println(sb.toString());
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files == null) return;
            for (File innerFiles : files) {
                printFolderTree(innerFiles , currentDepth + 1, maxDepth);
            }
        }
    }

}
