package homeworks.Testings;

import java.security.MessageDigest;

public class Encryptor {

    public static String encrypt(String str) {
        String hashValue = "";
        try {
          MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
          messageDigest.update(str.getBytes());
          byte[] encodedHash = messageDigest.digest(str.getBytes());
          hashValue = bytesToHex(encodedHash);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hashValue;
    }

    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }

}
