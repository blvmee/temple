package homeworks.Testings;

public class NotEnoughQuestionsException extends RuntimeException {

    public NotEnoughQuestionsException(String message) {
        super(message);
    }
}
