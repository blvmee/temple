package homeworks.Testings;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Quiz {

    static Scanner scan = new Scanner(System.in);
    static QuestionStorage questions;

    public static void main(String[] args) {
        setQuestionsUp();
        System.out.println("The test will start shortly, please enter the amount of questions asked");
        Question[] testQuestions = questions.getQuestions(scan.nextInt());
        startTest(testQuestions);
    }

    static void setQuestionsUp() {
        questions = new QuestionStorage();
        if (questions.getSize() == 0) {
            System.out.println("No questions from previous sessions were found, you will need to add some questions manually:");
        } else {
            System.out.println("Found " + questions.getSize() + " questions from previous sessions");
            System.out.println("You can still add some questions manually:");
        }
        System.out.println("Enter amount of new questions");
        questions.fillManually(scan.nextInt());
        System.out.println("Done adding questions.");
    }


    static void startTest(Question[] tasks) {
        long startTimestamp = new Date().getTime();
        ArrayList<Question> incorrect = new ArrayList<>();
        int studentScore = 0;
        int maxScore = 0;
        for (Question q : tasks) {
            q.displayQuestion();
            System.out.println(q.getAmountOfAnswers() == 1 ? "Pick an answer" : "Enter numbers of correct options");
            int pointsGained = q.checkAnswer(scan.nextLine().split(" "));
            if (pointsGained != q.getValue()) incorrect.add(q);
            studentScore += pointsGained;
            maxScore += q.getValue();
        }
        long endTimestamp = new Date().getTime();
        Date timePassed = new Date(endTimestamp - startTimestamp);
        System.out.println("It took you " + timePassed.getMinutes() + " minutes "+ timePassed.getSeconds() + " seconds to complete the test");
        System.out.println("Your score: " + studentScore + "/" + maxScore);
        System.out.println("You failed to answer these questions:");
        for (Question question : incorrect) {
            question.printWithAnswers();
        }
    }


}
