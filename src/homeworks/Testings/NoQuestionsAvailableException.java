package homeworks.Testings;

public class NoQuestionsAvailableException extends NotEnoughQuestionsException {

    public NoQuestionsAvailableException(String message) {
        super(message);
    }
}
