package homeworks.GraphTasks.oopgraphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KruskalAlgorithm {

    int minWeight;

    public KruskalAlgorithm(Graph g) {
        minWeight = solve(g);
    }

    private int solve(Graph g) {
        int minWeight = 0;
        int n  = g.getVertexCount();

        Edge[] sortedEdges = g.getEdgesArray();
        Arrays.sort(sortedEdges);

        List<Edge> chosenEdges = new ArrayList<>();

        int[] nodeComponent = new int[n];
        for (int i = 0; i < n; i++) {
            nodeComponent[i] = i;
        }

        int i = 0;
        while (chosenEdges.size() != n - 1) {
            Edge cheapestEdge = sortedEdges[i];
            int node1 = cheapestEdge.getFirstNode().getId();
            int node2 = cheapestEdge.getSecondNode().getId();
            i++;
            if (nodeComponent[node1] == nodeComponent[node2]) continue;
            int prevNodeComponent = nodeComponent[node2];
            for (int j = 0; j < nodeComponent.length; j++) {
                if (nodeComponent[j] == prevNodeComponent) {
                    nodeComponent[j] = nodeComponent[node1];
                }
            }
            chosenEdges.add(cheapestEdge);
            minWeight += cheapestEdge.getWeight();
        }
        return minWeight;
    }

    public int getMinWeight() {
        return minWeight;
    }
}
