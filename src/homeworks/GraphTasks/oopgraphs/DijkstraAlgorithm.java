package homeworks.GraphTasks.oopgraphs;

import java.util.*;

public class DijkstraAlgorithm {

    private int[] shortestDistances;
    private ArrayDeque<Vertex>[] shortestPaths;

    public DijkstraAlgorithm(Digraph g, int startIndex) {
        
        shortestDistances = new int[g.getVertexCount()];
        Arrays.fill(shortestDistances, Integer.MAX_VALUE);

        shortestPaths = new ArrayDeque[g.getVertexCount()];
        for (int i = 0; i < g.getVertexCount(); i++) {
            shortestPaths[i] = new ArrayDeque<>();
        }

        solve(g, startIndex);
    }
    
    private void solve(Digraph graph, int startIndex) {
        boolean[] visitedVertices = new boolean[graph.getVertexCount()];

        PriorityQueue<Arc> arcsToTravel = new PriorityQueue<>();
        arcsToTravel.add(new Arc(Vertex.DUMMY_VERTEX, graph.getVertex(startIndex), Integer.MAX_VALUE));

        shortestDistances[startIndex] = 0;
        shortestPaths[startIndex].add(graph.getVertex(startIndex));

        while (!arcsToTravel.isEmpty()) {
            Vertex focus = arcsToTravel.poll().getDestination();
            int focusId = focus.getId();
            if (visitedVertices[focusId]) continue;

            // overview adjacent vertices
            List<Arc> incidentArcs = focus.getArcsWithOriginInThisVertex();
            for (Arc arc : incidentArcs) {
                // write down distances
                Vertex destination = arc.getDestination();
                int destinationId = destination.getId();
                if (shortestDistances[destinationId] > shortestDistances[focusId] + arc.getWeight()) {
                    shortestDistances[destinationId] =  shortestDistances[focusId] + arc.getWeight();
                    shortestPaths[destinationId] = new ArrayDeque<>(shortestPaths[focusId]);
                    shortestPaths[destinationId].add(destination);
                }
            }

            // set closest vertex
            Arc minArc = Arc.BIG_DUMMY_ARC;
            for (Arc arc : incidentArcs) {
                if (arc.getWeight() < minArc.getWeight()) minArc = arc;
            }
            if (minArc == Arc.BIG_DUMMY_ARC) continue;

            // go to closest vertex
            arcsToTravel.addAll(incidentArcs);
            visitedVertices[focusId] = true;

        }
//        for (int i = 0; i < shortestDistances.length; i++) {
//            if (shortestDistances[i] == Integer.MAX_VALUE) shortestDistances[i] = -1;
//        }
    }

    public HashMap<Integer, Integer> resultAsHashMap() {
        HashMap<Integer, Integer> shortestDistancesAsAMap = new HashMap<>();
        for (int i = 0; i < shortestDistances.length; i++) {
            shortestDistancesAsAMap.put(i, shortestDistances[i]);
        }
        return shortestDistancesAsAMap;
    }

}
