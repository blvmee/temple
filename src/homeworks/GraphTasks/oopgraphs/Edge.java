package homeworks.GraphTasks.oopgraphs;

import java.util.Arrays;

public class Edge implements Comparable<Edge> {

    private Vertex[] nodes;
    private int weight;

    public Edge(Vertex node1, Vertex node2, int weight) {
        this.nodes = new Vertex[]{node1, node2};
        this.weight = weight;
        node1.addEdge(this);
        node2.addEdge(this);
    }

    public Vertex getFirstNode() {
        return nodes[0];
    }

    public Vertex getSecondNode() {
        return nodes[1];
    }

    public int getWeight() {
        return weight;
    }

    public int compareTo(Edge edge) {
        return Integer.compare(this.getWeight(), edge.getWeight());
    }

    @Override
    public String toString() {
        return "Edge{" +
                "nodes=" + Arrays.toString(nodes) +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Edge) return ((Edge) obj).getFirstNode() == getFirstNode()
                && ((Edge) obj).getSecondNode() == getSecondNode()
                && ((Edge) obj).getWeight() == getWeight();
        return super.equals(obj);
    }
}
