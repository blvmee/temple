package homeworks.GraphTasks.oopgraphs;

import java.util.ArrayList;
import java.util.HashMap;

public class Digraph {

    private final HashMap<Integer, Vertex> vertices;
    private final ArrayList<Arc> arcs = new ArrayList<>();

    public Digraph(int[][] adjacencyMatrix) {
        // create vertices
        int VertexCount = adjacencyMatrix.length;
        HashMap<Integer, Vertex> vertices = new HashMap<>();
        for (int i = 0; i < VertexCount; i++) {
            vertices.put(i, new Vertex(i));
        }
        // create arcs
        for (int i = 0; i < VertexCount; i++) {
            for (int j = 0; j < VertexCount; j++) {
                if (adjacencyMatrix[i][j] > 0) {
                    int arcWeight = adjacencyMatrix[i][j];
                    arcs.add(new Arc(vertices.get(i), vertices.get(j), arcWeight));
                }
            }
        }
        this.vertices = vertices;
    }

    public int getVertexCount() {
        return vertices.size();
    }

    public Vertex getVertex(Integer key) {
        return vertices.get(key);
    }

}
