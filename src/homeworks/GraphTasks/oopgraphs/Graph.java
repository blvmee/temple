package homeworks.GraphTasks.oopgraphs;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph {

    private final HashMap<Integer, Vertex> nodes;
    private final ArrayList<Edge> edges = new ArrayList<>();

    public Graph(int[][] adjacencyMatrix) {
        // create nodes
        int VertexCount = adjacencyMatrix.length;
        HashMap<Integer, Vertex> nodes = new HashMap<>();
        for (int i = 0; i < VertexCount; i++) {
            nodes.put(i, new Vertex(i));
        }
        // create arcs
        for (int i = 0; i < VertexCount; i++) {
            for (int j = i; j < VertexCount; j++) {
                if (adjacencyMatrix[i][j] > 0) {
                    int arcWeight = adjacencyMatrix[i][j];
                    edges.add(new Edge(nodes.get(i), nodes.get(j), arcWeight));
                }
            }
        }
        this.nodes = nodes;
    }

    public Edge[] getEdgesArray() {
        Edge[] result = new Edge[edges.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = edges.get(i);
        }
        return result;
    }

    public Vertex getVertex(int index) {
        return nodes.get(index);
    }

    public int getVertexCount() {
        return nodes.size();
    }

}
