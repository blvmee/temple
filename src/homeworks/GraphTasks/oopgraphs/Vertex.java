package homeworks.GraphTasks.oopgraphs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Vertex {

    public static final Vertex DUMMY_VERTEX = new Vertex(-1);

    private final int id;

    public Vertex(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "id=" + id +
                '}';
    }



    /**
     *
     *                        For graphs
     *
     */



    Set<Edge> edges = new HashSet<>();

    public void addEdge(Edge edge) {
        if (!edges.add(edge)) return;
        Vertex nodeToAdd = edge.getFirstNode() == this ? edge.getSecondNode() : edge.getFirstNode();
    }

    public ArrayList<Edge> getEdges() {
        return new ArrayList<>(edges);
    }



    /**
     *
     *                        For digraphs
     *
     */



    Set<Arc> arcs = new HashSet<>();

    private final ArrayList<Arc> arcsWithOriginInThisVertex = new ArrayList<>();

    public void addStartingArc(Arc arc) {
        if (!arcs.add(arc)) return;
        arcsWithOriginInThisVertex.add(arc);
    }

    public List<Arc> getArcsWithOriginInThisVertex() {
        return arcsWithOriginInThisVertex;
    }

}
