package homeworks;

import java.util.Scanner;

public class codeforces1365A {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int amountOfInputs = scan.nextInt();
        String[] output = new String[amountOfInputs];
        for (int p = 0; p < amountOfInputs; p++) {
            int[][] board = new int[scan.nextInt()][scan.nextInt()];
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    board[i][j] = scan.nextInt();
                }
            }
            int count = 0;
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    if (board[i][j] == 0) {
                        boolean cageIsValid = true;
                        for (int m = 0; m < board.length; m++) {
                            if (board[m][j] != 0) {
                                cageIsValid = false;
                                break;
                            }
                        }
                        for (int n = 0; n < board[i].length; n++) {
                            if (board[i][n] != 0) {
                                cageIsValid = false;
                                break;
                            }
                        }
                        if (cageIsValid) {
                            count++;
                            board[i][j] = 1;
                        }
                    }
                }
            }
            output[p] = count % 2 == 0 ? "Vivek" : "Ashish";
        }
        for (String s : output) {
            System.out.println(s);
        }
    }

}
