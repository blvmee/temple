package homeworks.dz1;

import java.util.Scanner;

class dzOneThree{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter width..");
		int n = scanner.nextInt();
		System.out.print("Enter height..");
		int m = scanner.nextInt();
		System.out.print("Enter a..");
		int a = scanner.nextInt();
		int cnt = 0;
		int pieces = n/a; //горизонтально нарезаем прямоугольник на участки, высота участка соответствует стороне квадрата
		if(n%a > 0){    
			pieces++;
		}
		while(pieces>0){ //обрабатываем участки, считаем сколько квадратов нужно чтобы заполнить участок вдоль
			if(m%a==0){
				cnt+=m/a;
			}else{
				cnt+=m/a;
				cnt++;
			}
			pieces--;
		}
		System.out.println(cnt);
	}
}