package homeworks.dz2;

import java.util.Scanner;
class dzTwo3{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter size..");
		int size = scanner.nextInt();
		int[] a = new int[size];
		System.out.println("Fill array:");
		for(int i = 0;i<a.length;i++){
			System.out.print("["+i+"] ");
			a[i]=scanner.nextInt();
		}
		System.out.print("Input number...");
		int x = scanner.nextInt();
		boolean isFound=false;
		for(int i=0;i<a.length;i++){
			if(a[i]==x){
				System.out.println("index = "+i);
				isFound=true;
				break;
			}
		}
		if(isFound==false){
			System.out.println("index not found");
		}
	}
}