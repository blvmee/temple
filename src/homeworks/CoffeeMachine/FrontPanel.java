package homeworks.CoffeeMachine;

import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class FrontPanel {

    private static final int WIDTH = 750;
    private static final int HEIGHT = 500;

    static String backgroundPath = (getPath() + "background.png");
    static ImageIcon backgroundPng = new ImageIcon(backgroundPath);

    public static void main(String[] args) {
        CoffeeMachineControlPanel.handleMenu();
        JLabel mainBackground = new JLabel(backgroundPng, 0);
        JFrame frontPanel = new JFrame("Control Panel");
        frontPanel.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frontPanel.setSize(WIDTH, HEIGHT);
        frontPanel.setResizable(false);
        frontPanel.setLocationRelativeTo(null);
        frontPanel.setIconImage(CoffeeMachine.appIcon.getImage());
        frontPanel.add(mainBackground);
        frontPanel.setVisible(true);
        frontPanel.add(new Immersion());
    }

    public static String getPath() {
        return System.getProperty("user.dir") + "\\src\\homeworks\\CoffeeMachine\\source\\";
    }

}
