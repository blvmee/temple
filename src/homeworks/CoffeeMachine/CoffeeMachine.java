package homeworks.CoffeeMachine;

import javax.swing.ImageIcon;

public class CoffeeMachine {

    public static ImageIcon appIcon = new ImageIcon(FrontPanel.getPath() + "icon.png");

    private int water;
    private int milk;
    private int sugar;
    private int coffeeBeans;
    private String syrup = "none";
    private boolean isOn;
    private boolean milkIsSteamed;

    public boolean enoughResourcesToCook(Coffee coffee) {
        if (this.coffeeBeans - coffee.beansNeeded < 0) return false;
        if (this.water - coffee.waterNeeded < 0) return false;
        if (this.milk - coffee.milkNeeded < 0) return false;
        if ((!this.milkIsSteamed) & (coffee.steamedMilkIsNeeded)) return false;
        this.coffeeBeans -= coffee.beansNeeded;
        this.water -= coffee.waterNeeded;
        this.milk -= coffee.milkNeeded;
        if (this.milk == 0) {
            this.milkIsSteamed = false;
        }
        return true;
    }

    public String steamMilk() {
        if (!this.milkIsSteamed) {
            if (this.milk > 0) {
                this.milkIsSteamed = true;
                return "Milk is now steamed.";
            } else {
                return "No milk to steam!";
            }
        } else {
            return "Milk is already steamed!";
        }
    }

    @Override
    public String toString() {
        return ("Current state :\n" +
                "Beans = " + this.coffeeBeans + "g\n" +
                "Water = " + this.water + "ml\n" +
                "Sugar = " + this.sugar + "g\n" +
                "Milk = " + this.milk + "ml\n" +
                "Milk is steamed = " + this.milkIsSteamed + "\n" +
                "Syrup = " + this.syrup + "\n");
    }

    public void setSyrup(String syrup) {
        this.syrup = syrup;
    }

    public String getSyrup() {
        if (this.syrup.equals("none")) {
            return "";
        }
        return (" with " + this.syrup);
    }

    public void setOn(boolean on) {
        this.isOn = on;
    }

    public boolean getOn() {
        return this.isOn;
    }

    public void setMilk(int milk) {
        this.milk += milk;
    }

    public int getMilk() {
        return this.milk;
    }

    public void setWater(int water) {
        this.water += water;
    }

    public int getWater() {
        return this.water;
    }

    public void setSugar(int sugar) {
        this.sugar += sugar;
    }

    public int getSugar() {
        return this.sugar;
    }

    public int getCoffeeBeans() {
        return coffeeBeans;
    }

    public void setCoffeeBeans(int coffeeBeans) {
        this.coffeeBeans += coffeeBeans;
    }
}