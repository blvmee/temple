package homeworks.CoffeeMachine;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

public class CoffeePicker extends JPanel {

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        displayControlButtons();
    }

    static int buttonCount = CoffeeMachineControlPanel.getListSize();

    public void displayControlButtons() {
        JButton startButton = new JButton("Start");
        startButton.setBounds(100, 25, 350, 50);
        add(startButton);
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CoffeeMachineControlPanel.tryStartingCookup();
            }
        });
        JButton[] chooseTypeButtons = new JButton[buttonCount];
        int i = 0;
        for (int y = 100; y < 524; y += 75) {
            for (int x = 25; x < 376; x += 175) {
                if (i < buttonCount) {
                    String title = CoffeeMachineControlPanel.coffeeList.get(i).naming;
                    chooseTypeButtons[i] = new JButton(title);
                    chooseTypeButtons[i].setBounds(x, y, 150, 50);
                    add(chooseTypeButtons[i]);
                    final int finalI = i;
                    chooseTypeButtons[i].addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            CoffeeMachineControlPanel.setTypeOfCoffee(finalI);
                        }
                    });
                    i++;
                }
            }
        }
    }

}
