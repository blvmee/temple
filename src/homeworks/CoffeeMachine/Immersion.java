package homeworks.CoffeeMachine;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Immersion extends JPanel {

    public void paintComponent(Graphics g) {
        setLayout(null);
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        Font pont = new Font("Serif", Font.BOLD, 24);
        g.setFont(pont);
        g.drawString("Solevarnya Inc. 2020", 5, 445);
        pont = new Font("Serif", Font.ITALIC, 30);
        g.setFont(pont);
        g.drawString("Hot Coffee Maker", 10, 30);
        handleButtons();
    }

    private final String[] arrOfButtonNames = {"Load Beans", "Load Sugar", "Show Recipes..", "Pour Water", "Add Syrup", "Check Resources", "Pour Milk", "Steam Milk", "Add Custom Recipe"};
    private JButton[] arrOfButtons = new JButton[9];

    void handleButtons() {
        for (int i = 0; i < 9; i++) {
            arrOfButtons[i] = new JButton(arrOfButtonNames[i]);
            int finalI = i;
            arrOfButtons[i].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    CoffeeMachineControlPanel.generalControls(finalI);
                }
            });
        }
        drawControlButtons();
    }

    static int pickerWidth = 565;
    static int pickerHeight = 600;

    static String pickerBackgroundPath = FrontPanel.getPath() + "cpBack.png";
    static ImageIcon pickerBackground = new ImageIcon(pickerBackgroundPath);

    void drawControlButtons() {
        JButton onOffButton = new JButton("Power On/Off");
        onOffButton.setBounds(25, 100, 150, 100);
        add(onOffButton);
        onOffButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CoffeeMachineControlPanel.generalControls(9);
            }
        });
        JButton startCookingButton = new JButton("Start cooking..");
        startCookingButton.setBounds(275, 350, 350, 75);
        add(startCookingButton);
        startCookingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (CoffeeMachineControlPanel.tryStarting()) {
                    JLabel coffeePickerBackground = new JLabel(pickerBackground, 0);
                    JFrame coffeePicker = new JFrame("Choose your coffee");
                    coffeePicker.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    coffeePicker.setSize(pickerWidth, pickerHeight);
                    coffeePicker.setResizable(false);
                    coffeePicker.setLocationRelativeTo(null);
                    coffeePicker.setIconImage(CoffeeMachine.appIcon.getImage());
                    coffeePicker.add(coffeePickerBackground);
                    coffeePicker.setVisible(true);
                    coffeePicker.add(new CoffeePicker());
                }
            }
        });
        int i = 0;
        for (int x = 200; x < 551; x += 175) {
            for (int y = 50; y < 251; y += 100) {
                JButton button = arrOfButtons[i];
                button.setBounds(x, y, 150, 75);
                add(button);
                i++;
            }
        }
    }
}
