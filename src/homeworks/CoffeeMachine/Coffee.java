package homeworks.CoffeeMachine;

public class Coffee {

    public String naming;
    public int beansNeeded;
    public int waterNeeded;
    public int milkNeeded;
    public boolean isLarge;
    public boolean steamedMilkIsNeeded;

    public Coffee(int beansNeeded, int waterNeeded, int milkNeeded, boolean isLarge, boolean steamedMilkIsNeeded, String naming) {
        this.beansNeeded = beansNeeded;
        this.waterNeeded = waterNeeded;
        this.milkNeeded = milkNeeded;
        this.isLarge = isLarge;
        this.steamedMilkIsNeeded = steamedMilkIsNeeded;
        this.naming = naming;
    }

    @Override
    public String toString() {
        return (this.naming + "; " +
                "Beans needed = " + this.beansNeeded + "; " +
                "Water needed = " + this.waterNeeded + "; " +
                "Milk needed = " + this.milkNeeded + "; " +
                "Steamed milk needed - " + this.steamedMilkIsNeeded);
    }

}
