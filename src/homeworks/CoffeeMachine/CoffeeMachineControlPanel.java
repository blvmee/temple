package homeworks.CoffeeMachine;

import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class CoffeeMachineControlPanel {

    static CoffeeMachine machine = new CoffeeMachine();

    static Coffee emptyDrink = new Coffee(0, 0, 0, false, false, "");

    static ArrayList<Coffee> coffeeList = new ArrayList();

    public static int getListSize() {
        return coffeeList.size();
    }

    static String type;
    static int typeID;
    static boolean typeIsPicked;

    static void setTypeOfCoffee(int i) {
        typeID = i;
        typeIsPicked = true;
        type = coffeeList.get(i).naming;
        System.out.println("You chose " + type + ".");
    }

    static String getTypeOfCoffee() {
        return type;
    }

    static String[] strYesNo = {"No", "Yes"};

    static String[] syrups = {"Caramel Syrup", "Honey syrup", "Cinnamon Syrup", "Vanilla syrup"};

    public static void handleMenu() {
        coffeeList.add(new Coffee(100, 150, 60, false, true, "Espresso Small"));
        coffeeList.add(new Coffee(170, 225, 100, true, true, "Espresso Large"));
        coffeeList.add(new Coffee(100, 90, 60, false, true, "Cappuccino Small"));
        coffeeList.add(new Coffee(150, 140, 100, true, true, "Cappuccino Large"));
        coffeeList.add(new Coffee(100, 100, 60, false, true, "Americano Small"));
        coffeeList.add(new Coffee(150, 250, 100, true, true, "Americano Large"));
        coffeeList.add(new Coffee(100, 160, 0, false, false, "Black Coffee Small"));
        coffeeList.add(new Coffee(150, 225, 0, true, false, "Black Coffee Large"));
        coffeeList.add(new Coffee(130, 100, 70, false, true, "Mocaccino"));
    }

    private static void show(Object something) {
        JOptionPane.showMessageDialog(null, something);
    }

    static String syrupPath = (FrontPanel.getPath() + "syrup.png");
    static ImageIcon syrupPng = new ImageIcon(syrupPath);
    static ImageIcon axax = new ImageIcon(FrontPanel.getPath() + "axax.jpg");

    public static int pwrSwitch;

    static void generalControls(int input) throws NumberFormatException {
        int t;
        if (!machine.getOn() & input != 9) {
            show("First, turn on the machine.");
        } else {
            try {
                switch (input) {
                    case 0:
                        t = parseAbs("Add coffee beans (g)..");
                        machine.setCoffeeBeans(t);
                        show("You added " + t + " g of coffee beans.");
                        break;
                    case 1:
                        t = parseAbs("Add sugar (g)..");
                        machine.setSugar(t);
                        show("You added " + t + " g of sugar.");
                        break;
                    case 2:
                        StringBuilder info = new StringBuilder("Supported types:\n");
                        for (Coffee coffee : coffeeList) {
                            info.append(coffee.toString()).append("\n");
                        }
                        show(info.toString());
                        break;
                    case 3:
                        t = parseAbs("Pour water (ml)..");
                        machine.setWater(t);
                        show("You poured in " + t + " ml of water.");
                        break;
                    case 4:
                        String syrup = (String) JOptionPane.showInputDialog(null, "What syrup do you prefer?", "Choose a syrup", JOptionPane.WARNING_MESSAGE, syrupPng, syrups, syrups[0]);
                        if (syrup != null) machine.setSyrup(syrup);
                        break;
                    case 5:
                        show(machine.toString());
                        break;
                    case 6:
                        t = parseAbs("Pour milk (ml)..");
                        machine.setMilk(t);
                        show("You poured in " + t + " ml of milk.");
                        break;
                    case 7:
                        show(machine.steamMilk());
                        break;
                    case 8:
                        if (CoffeePicker.buttonCount < 18) {
                            Coffee coffeeToAdd = createCustomRecipe();
                            if (!coffeeToAdd.equals(emptyDrink)) {
                                coffeeList.add(coffeeToAdd);
                                CoffeePicker.buttonCount++;
                            }
                        } else {
                            show("The machine is out of memory! Great job!");
                            selfDestruction();
                        }
                        break;
                    case 9:
                        machine.setOn(pwrSwitch % 2 == 0);
                        String ONorOFF = machine.getOn() ? "on." : "turned off.";
                        show("Coffee machine is now " + ONorOFF);
                        pwrSwitch++;
                        break;
                }
            } catch (NumberFormatException exception) {
                show(axax);
            }
        }
    }

    static Coffee createCustomRecipe() {
        Coffee newCoffee = new Coffee(0, 0, 0, false, false, null);
        newCoffee.naming = JOptionPane.showInputDialog(null, "Enter new recipe's name");
        newCoffee.beansNeeded = tryParsing("Enter coffee beans requirement..");
        newCoffee.waterNeeded = tryParsing("Enter water requirement..");
        newCoffee.milkNeeded = tryParsing("Enter milk requirement..");
        newCoffee.steamedMilkIsNeeded = "Yes".equals((JOptionPane.showInputDialog(null, "Does the drink require steamed milk?", "huh?", JOptionPane.WARNING_MESSAGE, null, strYesNo, strYesNo[0])));
        newCoffee.isLarge = "Yes".equals((JOptionPane.showInputDialog(null, "Finally, is it the large portion of the drink?", "huh?", JOptionPane.WARNING_MESSAGE, null, strYesNo, strYesNo[0])));
        if ((newCoffee.beansNeeded == -1 || newCoffee.waterNeeded == -1 || newCoffee.milkNeeded == -1) | (newCoffee.beansNeeded == 0 || newCoffee.waterNeeded == 0)) {
            show("Try again.");
            return emptyDrink;
        } else {
            show(newCoffee.naming + " recipe added successfully.");
            return newCoffee;
        }
    }

    static int tryParsing(String message) {
        try {
            return Integer.parseInt(JOptionPane.showInputDialog(null, message));
        } catch (NumberFormatException ignored) {
        }
        return -1;
    }

    static int parseAbs(String str) {
        int x = Integer.parseInt(JOptionPane.showInputDialog(str));
        x = x < 0 ? 0 : x;
        return x;
    }

    static boolean tryStarting() {
        if (!machine.getOn()) {
            show("You forgot to turn on the machine.");
            return false;
        }
        if (machine.getWater() == 0) {
            show("The machine is out of water.");
            return false;
        }
        if (machine.getCoffeeBeans() == 0) {
            show("Coffee machine can not operate without coffee beans.");
            return false;
        }
        return true;
    }

    static String cooperPath = (FrontPanel.getPath() + "cooper.png");
    static ImageIcon cooper = new ImageIcon(cooperPath);

    static void tryStartingCookup() {
        if (!typeIsPicked) {
            show("Choose a drink first!");
        } else {
            if (machine.enoughResourcesToCook(coffeeList.get(typeID))) {
                int sugar = tryParsing("Add sugar? (g)");
                if (sugar > machine.getSugar()) {
                    show("Not enough sugar!");
                } else {
                    machine.setSugar(-sugar);
                }
                JOptionPane.showMessageDialog(null, "Your " + getTypeOfCoffee() + machine.getSyrup() + " is ready.\nHave a nice one!", "Cooper says", JOptionPane.INFORMATION_MESSAGE, cooper);
                machine.setSyrup("none");
            } else {
                show("Not enough resources to cook.\nUse 'Show Recipes' and 'Check Resources' to inspect.");
            }
        }
    }

    private static void selfDestruction() {
        System.exit(0);
    }

}
