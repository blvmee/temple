package lessons.annotations;

public class OrdinaryJoe extends Joe {

    public OrdinaryJoe() {}

    public OrdinaryJoe(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "OrdinaryJoe{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", socialRating=" + socialRating +
                '}';
    }
}
