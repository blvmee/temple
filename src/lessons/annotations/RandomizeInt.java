package lessons.annotations;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RandomizeInt {

    int lowerBound() default 0;
    int upperBound() default Integer.MAX_VALUE;

}
