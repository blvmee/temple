package lessons.annotations;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface RandomizeJoeList {

    int maxSize() default 5;
    Class<? extends Joe> desiredClass() default OrdinaryJoe.class;

}
