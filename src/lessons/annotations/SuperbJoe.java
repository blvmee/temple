package lessons.annotations;

import java.util.List;

public class SuperbJoe extends Joe {

    @RandomizeFriendJoe
    private FemaleJoe wife;

    @RandomizeJoeList(maxSize = 10)
    private List<Joe> slaves;

    public SuperbJoe() {}

    public SuperbJoe(String name) {
        super(name);
        salary += 100000;
        socialRating = Math.abs(socialRating);
    }

    @Override
    public String toString() {
        return "SuperbJoe{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", socialRating=" + socialRating +
                ", wife=" + wife +
                ", slaves=" + slaves +
                '}';
    }
}
