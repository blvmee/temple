package lessons.annotations;

public class MiniJoe extends Joe {

    public MiniJoe() {}

    public MiniJoe(String name) {
        super(name);
        age = age % 17;
        socialRating = 0;
    }

    @Override
    public String toString() {
        return "MiniJoe{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", socialRating=" + socialRating +
                '}';
    }

}
