package lessons.annotations;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.*;

public class JoeFactory {

    public static void main(String[] args) throws Exception {
        new JoeFactory().generate(25, "lessons.annotations.Joe")
                .forEach(System.out::println);
    }

    public JoeFactory() throws FileNotFoundException {
        names = getRandomNames();
    }

    public List<Joe> generate(int amount, String joeClassName) {
        try {
            return generate(amount, Class.forName(joeClassName));
        } catch (ClassNotFoundException e) {
            System.err.println("Клас не нашелся");
            return null;
        }
    }

    public Joe createJoeByClass(String className) {
        try {
            return createJoeByClass(Class.forName(className));
        } catch (ClassNotFoundException e) {
            System.err.println("Клас не нашелся");
            return null;
        }
    }

    private List<Joe> generate(int joeAmount, Class<?> joeClass) {
        if (joeClass != Joe.class) {
            Class<?> parent;
            do {
                 parent = joeClass.getSuperclass();
                 if (parent == Joe.class) {
                     break;
                 }
             } while (parent != Object.class);

            if (parent == Object.class) {
                System.err.println("Не тот клас");
                return null;
            }
        }

        List<Joe> result = new LinkedList<>();

        Class<?>[] randomClasses = {OrdinaryJoe.class, SuperbJoe.class, FemaleJoe.class};
        Random rand = new Random();
        for (int i = 0; i < joeAmount; i++) {
            if (joeClass == Joe.class) {
                result.add(createJoeByClass(randomClasses[rand.nextInt(3)]));
            } else {
                result.add(createJoeByClass(joeClass));
            }
        }
        return result;
    }

    private Joe createJoeByClass(Class<?> joeClass) {
        Object joe;
        try {
            joe = joeClass.getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException e) {
            System.err.println("Что-то пошло не так) ");
            return null;
        }
        process(joe);
        return (Joe) joe;
    }

    private void process(Object o) {
        this.getAllFields(o.getClass())
                .forEach(field -> {
                    try {
                        if (field.getName().equals("name")) field.set(o, getRandomName());
                        // randomize ints
                        var rInt = field.getDeclaredAnnotation(RandomizeInt.class);
                        if (rInt != null) {
                            field.set(o, new Random().nextInt(rInt.upperBound() - rInt.lowerBound()) + rInt.lowerBound());
                        }
                        // randomize friend joe
                        var rFriend = field.getDeclaredAnnotation(RandomizeFriendJoe.class);
                        if (rFriend != null) {
                            Class<?> targetClass = field.getType() == Joe.class ? OrdinaryJoe.class : field.getType();
                            field.set(o, this.createJoeByClass(targetClass));
                        }
                        // randomize joe list
                        var rList = field.getDeclaredAnnotation(RandomizeJoeList.class);
                        if (rList != null) {
                            field.set(o, this.generate(rList.maxSize(), rList.desiredClass().getName()));
                        }
                    } catch (IllegalAccessException cannotHappen) {
                    } catch (IllegalArgumentException e) {
                        System.err.println("неверный тип поля");
                    }
                });
    }

    private List<Field> getAllFields(Class<?> type) {
        List<Field> fields = new ArrayList<>();
        for (Class<?> c = type; c != Object.class; c = c.getSuperclass()) {
            fields.addAll(Arrays.asList(c.getDeclaredFields()));
        }
        fields.forEach(field -> field.setAccessible(true));
        return fields;
    }

    // naming

    public String getRandomName() {
        Collections.shuffle(names);
        return names.get(0);
    }

    private final List<String> names;

    private static List<String> getRandomNames() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("src/random/warfare/names.txt"));
        List<String> result = new ArrayList<>();
        while (scanner.hasNext()) {
            result.add(scanner.nextLine());
        }
        return result;
    }

}
