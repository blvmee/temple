package lessons.annotations;

import java.util.List;

public class FemaleJoe extends Joe {

    @RandomizeFriendJoe
    private Joe husband;

    @RandomizeJoeList(maxSize = 4, desiredClass = MiniJoe.class)
    private List<Joe> kids;

    public FemaleJoe() {}

    public FemaleJoe(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "FemaleJoe{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", socialRating=" + socialRating +
                ", husband=" + husband +
                ", kids=" + kids +
                '}';
    }
}
