package lessons.annotations;

public abstract class Joe {

    protected String name;

    @RandomizeInt(lowerBound = 5, upperBound = 54)
    protected int age;

    @RandomizeInt(lowerBound = 20000, upperBound = 300000)
    protected int salary;

    @RandomizeInt(lowerBound = -1000, upperBound = 1000)
    protected int socialRating;

    public Joe() {}

    public Joe(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Joe{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", socialRating=" + socialRating +
                '}';
    }
}
