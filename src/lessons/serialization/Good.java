package lessons.serialization;

import java.io.Serializable;

public class Good implements Serializable {

    String name;
    int cost;

    public Good(String name, int cost) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Good{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                '}';
    }
}
