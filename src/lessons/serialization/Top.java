package lessons.serialization;

import java.io.Serializable;

public class Top extends Good  implements Serializable {

    String size;
    String color;

    public Top(String name, int cost, String size, String color) {
        super(name, cost);
        this.size = size;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Top{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", size='" + size + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
