package lessons.serialization;

import java.io.Serializable;

public class Shoes extends Good implements Serializable {

    int size;
    String color;
    String sex;

    public Shoes(String name, int cost, int size, String color, String sex) {
        super(name, cost);
        this.size = size;
        this.color = color;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Shoes{" +
                "name='" + name + '\'' +
                ", cost=" + cost +
                ", size=" + size +
                ", color='" + color + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
