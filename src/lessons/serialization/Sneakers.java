package lessons.serialization;

import java.io.Serializable;

public class Sneakers extends Shoes implements Serializable {

    int coolness;

    public Sneakers(String name, int cost, int size, String color, String sex, int coolness) {
        super(name, cost, size, color, sex);
        this.coolness = coolness;

    }
}

