package lessons.serialization;

import java.io.Serializable;

public class Slippers extends Shoes implements Serializable {

    public Slippers(String name, int cost, int size, String color, String sex) {
        super(name, cost, size, color, sex);
    }
}
