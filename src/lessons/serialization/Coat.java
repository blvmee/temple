package lessons.serialization;

import java.io.Serializable;

public class Coat extends Top implements Serializable {

    public Coat(String name, int cost, String size, String color) {
        super(name, cost, size, color);
    }
}
