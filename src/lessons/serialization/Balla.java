package lessons.serialization;

import java.io.Serializable;

public class Balla implements Serializable {

    int balance;
    ThriftShop destination;

    public Balla(ThriftShop destination) {
        balance = 20;
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "Balla{" +
                "balance=" + balance +
                ", destination=" + destination +
                '}';
    }
}
