package lessons.serialization;

import java.io.Serializable;

public class TShirt extends Top implements Serializable {

    public TShirt(String name, int cost, String size, String color) {
        super(name, cost, size, color);
    }

}
