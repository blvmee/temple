package lessons.serialization;

import java.io.*;
import java.util.*;

public class ThriftShop implements Serializable {

    List<Good> stock;

    public ThriftShop() {
        Random rand = new Random();
        Good[] stock = new Good[rand.nextInt(50)];
        for (int i = 0; i < stock.length; i++) {
            switch (rand.nextInt(5)) {
                case 0 -> stock[i] = new Sneakers("Velcros", rand.nextInt(200), 40, rand.nextBoolean() ? "black" : "white", rand.nextBoolean() ? "Male" : "Female", 60 + rand.nextInt(40));
                case 1 -> stock[i] = new Sneakers("Abibas", rand.nextInt(200), 42, "Brown", "Male", 0);
                case 2 -> stock[i] = new Shoes(rand.nextBoolean() ? "Oxfords" : "Brogues", rand.nextInt(200), 44, "Black", "Male");
                case 3 -> stock[i] = new Slippers("Tapki", rand.nextInt(200), 41, "Pink", rand.nextBoolean() ? "Male" : "Female");
                case 4 -> stock[i] = new TShirt("Putin best president", 10, "XL", "Yellow");
                case 5 -> stock[i] = new Coat("Chikatilo", rand.nextInt(200), "XL", "Grey");
                case 6 -> stock[i] = new Good("Broken keyboard", 50);
            }
        }
        this.stock = Arrays.asList(stock);
    }

    public static void main(String[] args) throws InterruptedException {
        next();
        Thread.sleep(1000);
    }

    private static void next() {
        try {
            FileWriter fw = new FileWriter("Zulul");
            BufferedWriter bfw = new BufferedWriter(fw);
            ThriftShop thriftShop = new ThriftShop();
            bfw.write(new Balla(thriftShop).toString());
            bfw.write(thriftShop.toString());
            bfw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void serialize() {
        ThriftShop shop = new ThriftShop();
        String path = System.getProperty("user.dir") + "\\src\\lessons\\s2e56\\files\\Shop.csv";
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(path);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(shop);

            objectOutputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void deserialize() {
        ThriftShop shop;
        try {
            FileInputStream fileInputStream = new FileInputStream(System.getProperty("user.dir") + "\\src\\lessons\\s2e56\\files\\Shop.csv");

            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

            shop = (ThriftShop) objectInputStream.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    void getInfo() {
        for (Good g : stock) {
            System.out.println(g.toString());
        }
    }


}
