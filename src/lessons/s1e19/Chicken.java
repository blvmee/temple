package lessons.s1e19;

public class Chicken {

    boolean isAlive;
    boolean isFried;
    boolean isMale;
    int size;
    ChickenStatus status;

    public Chicken(boolean isAlive, boolean isMale, boolean isFried, int size, ChickenStatus status) {
        this.isAlive = isAlive;
        this.isFried = isFried;
        this.isMale = isMale;
        this.size = size;
        this.status = status;
    }

    ChickenStatus getStatus() {
        return this.status;
    }

}

enum ChickenStatus {

    KFC,
    CASUAL,
    COCK,
    BIG,
    DEAD,
    SCORCHED

}
