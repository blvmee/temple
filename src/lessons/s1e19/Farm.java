package lessons.s1e19;

import java.util.ArrayList;
import java.util.Random;

public class Farm {

    static ArrayList<Chicken> theFarm = new ArrayList<>();
    static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {
        ChickenStatus state = ChickenStatus.COCK;
        for (int i = 0; i < 10; i++) {
            generateChics();
            int index = random.nextInt(theFarm.size());
            System.out.println("[" + index + "] " + reportEntityStatus(theFarm.get(index)));
            Thread.sleep(250);
        }

    }

    static void generateChics() {
        int i = random.nextInt(20);
        for (int j = 0; j < i; j++) {
            boolean isAlive = random.nextBoolean();
            boolean isFried = !isAlive && random.nextBoolean();
            boolean isMale = random.nextBoolean();
            int size = random.nextInt(5 + 1);
            ChickenStatus status = ChickenStatus.CASUAL;
            if (isAlive) {
                if (size == 5) {
                    status = ChickenStatus.BIG;
                } else {
                    if (isMale) {
                        status = ChickenStatus.COCK;
                    }
                }
            } else {
                if (isFried) {
                    if (size == 0) {
                        status = ChickenStatus.SCORCHED;
                    } else {
                        if (random.nextBoolean()) {
                            status = ChickenStatus.KFC;
                        } else {
                            status = ChickenStatus.DEAD;
                        }
                    }
                }
            }
            theFarm.add(new Chicken(isAlive,isFried,isMale,size,status));
        }
    }

    static String reportEntityStatus(Chicken chicken) {
        switch (chicken.getStatus()) {
            case KFC:
                return "Yummy!";
            case COCK:
                return "nice one, bro";
            case CASUAL:
                return "Alive and well";
            case DEAD:
                return "RIP";
            case SCORCHED:
                return "schewpid arse of mine thee madest";
            case BIG:
                return "Big";
            default:
                return "Dunno";
        }
    }

    static ArrayList<Chicken> searchForChicsByStatus(String state) {
        ArrayList<Chicken> result = new ArrayList<>();
        for (Chicken chicken : theFarm) {
            if (chicken.getStatus().toString().equals(state)) result.add(chicken);
        }
        return result;
    }

    // static ArrayList<Chicken>

}
