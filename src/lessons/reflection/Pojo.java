package lessons.reflection;

@Deprecated(since = "0")
public class Pojo implements Comparable<Pojo> {

    private String name = "Vladimir";
    private int health;
    public boolean is;

    public Pojo(String name, int health, boolean is) {
        this.name = name;
        this.health = health;
        this.is = is;
    }

    public void methodWithAnnotations() {
    }

    public Pojo() {
    }

    @Override
    public int compareTo(Pojo o) {
        return getHealth() - o.getHealth();
    }

    @Override
    public String toString() {
        return "Pojo{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", is=" + is +
                '}';
    }

    public int getHealth() {
        return health;
    }

    public void tryAndGetHealth() {
        System.out.println(getHealth());
    }

    private int sum(int a, int b) {
        return a + b;
    }

}
