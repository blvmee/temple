package lessons.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class FirstReflectionTest {

    public static void main(String[] args) throws Exception {
        new FirstReflectionTest();
    }

    public FirstReflectionTest() throws Exception {
        Pojo pojo = new Pojo();
        changePrivateField(pojo);

        callPrivateMethod(pojo);

        checkForAbstractAndFinal(SomeFinalClass.class);
        checkForAbstractAndFinal(SomeAbstractClass.class);

        createWithPrivateConstructor();
    }

    public void checkForObjectMethods(Pojo pojo) {
        System.out.println(Arrays.toString(pojo.getClass().getDeclaredMethods()));
        // only overridden
    }

    public void checkForObjectSuper(Pojo pojo) {
        System.out.println(pojo.getClass().getSuperclass());
    }

    public void checkForObjectSuper() {
        System.out.println(Pojo2.class.getSuperclass());
    }

    public void flipAbstractKeyword() {
        try {
            Class<SomeAbstractClass> someAbstractClassClass = SomeAbstractClass.class;
            System.out.println(someAbstractClassClass.getDeclaredConstructor().newInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void makeGetterPrivateAndBreakEverything() throws NoSuchMethodException {
        Pojo pojo = new Pojo("Hi", 20, true);
        Class<? extends Pojo> aClass = pojo.getClass();

        Method getHealth = aClass.getMethod("getHealth");
        getHealth.setAccessible(false);

        pojo.tryAndGetHealth();
    }

    public void changePrivateField(Pojo pojo) throws Exception {
        Field name = pojo.getClass().getDeclaredField("name");
        name.setAccessible(true);
        System.out.println(name.get(pojo)); // Vladimir
        name.set(pojo, "Putin");
        System.out.println(name.get(pojo)); // Putin
    }

    public void callPrivateMethod(Pojo pojo) throws Exception {
        Method sum = pojo.getClass().getDeclaredMethod("sum", int.class, int.class);
        sum.setAccessible(true);
        System.out.println(sum.invoke(pojo, 1 , 2));
    }

    public void checkForAbstractAndFinal(Class<?> clazz) {
        System.out.println(clazz.getName() + ":");
        System.out.println("is abstract - " + Modifier.isAbstract(clazz.getModifiers()));
        System.out.println("is final - " + Modifier.isFinal(clazz.getModifiers()));
    }

    public void createWithPrivateConstructor() throws Exception {
        Class<Pojo2> pojo2Class = Pojo2.class;
        Constructor<?> declaredConstructor = pojo2Class.getDeclaredConstructors()[0];
        declaredConstructor.setAccessible(true);
        Pojo2 pojo2 = (Pojo2) declaredConstructor.newInstance();
        System.out.println(pojo2);
    }

    public void getAnnotations(Pojo pojo) {
        System.out.println(Arrays.toString(pojo.getClass().getDeclaredAnnotations()));
    }

}
