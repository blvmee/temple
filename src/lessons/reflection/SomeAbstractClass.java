package lessons.reflection;

public abstract class SomeAbstractClass {

    String hi = "hi";

    @Override
    public String toString() {
        return "SomeAbstractClass{" +
                "hi='" + hi + '\'' +
                '}';
    }
}
