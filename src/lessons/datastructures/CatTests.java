package lessons.datastructures;

import org.junit.jupiter.api.*;

import java.util.LinkedList;
import java.util.List;

public class CatTests {

    List<Cat> src;

    ICatDataGetter impl1 = new Cats();
    ICatDataGetter impl2 = new CatsStreamImpl();

    
    @BeforeEach
    void createList() {
        src = new LinkedList<>();
        Cat dead = new Cat("dasjdjk", 1, null, null, null, false);
        Cat cat1 = new Cat("Vova", 10, dead ,dead, "kek", true);
        Cat cat2 = new Cat("Viva", 10, dead ,dead, "lol", true);
        Cat cat3 = new Cat("Arkadii", 9, cat2, cat1, "lol", false);
        Cat cat4 = new Cat("Alice", 14, cat1, cat2, "kek", true);
        Cat cat5 = new Cat("MicroChel", 2, cat1, cat4, "asd", true );
        Cat cat6 = new Cat("Alice111", 14, cat1, cat4, "asd", false);
        src.add(cat1);
        src.add(cat2);
        src.add(cat3);
//        src.add(cat4);
//        src.add(cat5);
//        src.add(cat6);
    }

    @AfterEach
    void destroyList() {
        src = null;
    }

    @Test
    void m1test() {
        Assertions.assertEquals(impl1.getCatsByAge(src), impl2.getCatsByAge(src));
    }

    @Test
    void m2test() {
        Assertions.assertEquals(impl1.getCatsByName(src), impl2.getCatsByName(src));
    }

    @Test
    void m3test() {
        Assertions.assertEquals(impl1.getCatsByTheirParent(src).entrySet(), impl2.getCatsByTheirParent(src).entrySet());
    }

    @Test
    void m4test() {
        Assertions.assertEquals(impl1.getCatsByWhetherTheyAreAliveOrNot(src), impl2.getCatsByWhetherTheyAreAliveOrNot(src));
    }
}
