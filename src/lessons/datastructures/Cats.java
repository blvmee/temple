package lessons.datastructures;

import java.util.*;

public class Cats implements ICatDataGetter {

    @Override
    public Map<String, Cat> getCatsByName(List<Cat> source) {
        Map<String, Cat> catsByName = new HashMap<>();
        for (Cat cat : source) {
            catsByName.put(cat.getName(), cat);
        }
        return catsByName;
        }

    @Override
    public Map<Integer, List<Cat>> getCatsByAge(List<Cat> source) {
        Map<Integer, List<Cat>> catsByAge = new HashMap<>();
        for (Cat cat: source)  {
            int age = cat.getAge();
            List<Cat> value = catsByAge.get(age);
            if (value == null) {
                List<Cat> notImmutableList = new ArrayList<Cat>();
                notImmutableList.add(cat);
                catsByAge.put(age, notImmutableList);
            } else {
                value.add(cat);
            }
        }
        return catsByAge;
    }

    @Override
    public Map<Cat, List<Cat>> getCatsByTheirParent(List<Cat> source) {
        Map<Cat, List<Cat>> catsByTheirParent = new HashMap<>();
        for (Cat cat : source) {
            if (cat.getMom() != null) {
                if (catsByTheirParent.containsKey(cat.getMom())) {
                    catsByTheirParent.get(cat.getMom()).add(cat);
                } else {
                    List<Cat> notImmutableList = new ArrayList<>();
                    notImmutableList.add(cat);
                    catsByTheirParent.put(cat.getMom(), notImmutableList);
                }
            }
            if (cat.getDad() != null) {
                if (catsByTheirParent.containsKey(cat.getDad())) {
                    catsByTheirParent.get(cat.getDad()).add(cat);
                } else {
                    List<Cat> notImmutableList = new ArrayList<>();
                    notImmutableList.add(cat);
                    catsByTheirParent.put(cat.getDad(), notImmutableList);
                }
            }
        }
        return catsByTheirParent;
    }

    @Override
    public Map<Boolean, List<Cat>> getCatsByWhetherTheyAreAliveOrNot(List<Cat> source) {
        Map<Boolean, List<Cat>> result = new HashMap<>();
        for (Cat cat : source) {
            boolean isAlive = cat.isAlive;
            List<Cat> value = result.get(isAlive);
            if (value == null) {
                List<Cat> notImmutableList = new ArrayList<>();
                notImmutableList.add(cat);
                result.put(isAlive, notImmutableList);
            } else {
                value.add(cat);
            }
        }
        return result;
    }
}
