package lessons.datastructures;

import java.util.List;
import java.util.Map;

public interface ICatDataGetter {

    Map<String, Cat> getCatsByName(List<Cat> source);

    Map<Integer, List<Cat>> getCatsByAge(List<Cat> source);

    Map<Cat, List<Cat>> getCatsByTheirParent(List<Cat> source);

    Map<Boolean, List<Cat>> getCatsByWhetherTheyAreAliveOrNot(List<Cat> source);

}
