package lessons.datastructures;

public class Cat {

    String name;
    int age;
    Cat dad;
    Cat mom;
    String color;
    boolean isAlive;

    public boolean isAlive() {
        return isAlive;
    }

    public Cat(String name, int age, Cat dad, Cat mom, String color, boolean isAlive) {
        this.name = name;
        this.age = age;
        this.dad = dad;
        this.mom = mom;
        this.color = color;
        this.isAlive = isAlive;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Cat getDad() {
        return dad;
    }

    public Cat getMom() {
        return mom;
    }

    public boolean inherits(Cat cat) {
        return dad == cat | mom == cat;
    }

    @Override
    public String toString() {
        return "Cat{" +
                name + '\'' +
                ", age=" + age +
                ", dad=" + dad +
                ", mom=" + mom +
                ", color='" + color + '\'' +
                '}' + '\n';
    }



}
