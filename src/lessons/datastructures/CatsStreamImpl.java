package lessons.datastructures;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CatsStreamImpl implements ICatDataGetter {

    @Override
    public Map<String, Cat> getCatsByName(List<Cat> source) {
        return source.stream()
                .collect(Collectors.toMap(Cat::getName, cat -> cat));
    }

    @Override
    public Map<Integer, List<Cat>> getCatsByAge(List<Cat> source) {
        return source.stream()
                .collect(Collectors.groupingBy(Cat::getAge));
    }

    @Override
    public Map<Cat, List<Cat>> getCatsByTheirParent(List<Cat> source) {
        return source.stream()
                .collect(Collectors.toMap(cat -> cat,
                        parentCat -> source.stream()
                                .filter(littleCat -> littleCat.inherits(parentCat))
                                .collect(Collectors.toList()))
                );
    }

    @Override
    public Map<Boolean, List<Cat>> getCatsByWhetherTheyAreAliveOrNot(List<Cat> source) {
        return source.stream()
                .collect(Collectors.groupingBy(Cat::isAlive));
    }

}
