package lessons.s1e17;

public class Dummy {

    static int foo;
    int bar;

    public Dummy() {
        foo++;

        this.bar = foo;
    }

    @Override
    public String toString() {
        return "gav gav" + bar;
    }
}
