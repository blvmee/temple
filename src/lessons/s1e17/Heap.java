package lessons.s1e17;

import java.util.ArrayList;
import java.util.Random;

public class Heap {

    static Random rand = new Random();

    public static void main(String[] args) {
        ArrayList<Dummy> piska = new ArrayList<>();
        boolean b = rand.nextBoolean();
        while (!b) {
            b = rand.nextBoolean();
        }
        try {
            while (b) {
                piska.add(new Dummy());
            }
        } catch (OutOfMemoryError error) {
            error.printStackTrace();
        }
        System.out.println(piska.get(piska.size() - 1).toString());
    }

}
