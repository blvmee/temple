package lessons.s1e17;

public class Stack {

    public static void main(String[] args) {
        System.out.println(fib(15));
        System.out.println(fact(10));
        try {
            overflow(1);
        } catch (StackOverflowError error) {
            System.out.println("ti eblan?");
        }
    }

    static int fib(int n) {
        if (n == 1 || n == 2) return 1;
        return fib(n-2) + fib(n-1);
    }

    static int fact(int f) {
        if (f == 0) return 1;
        return f*fact(f-1);
    }

    static void overflow(int n) {
        System.out.println(n);
        overflow(n + 1);
    }

}
