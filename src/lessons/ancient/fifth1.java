package lessons.ancient;

import java.util.Scanner;
class fifth1{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int d = sc.nextInt();
		int cnt = 0;
		while(d>4){
			cnt++;
			d-=5;
		}
		System.out.print(d==0 ? cnt : cnt+1 );
	}
}