package lessons.ancient;

import java.util.Scanner;

class third2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter size..");
        int size = scanner.nextInt();
        size--;
        int[] array = new int[size];
        System.out.println("Input numbers:");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int sum = 0;
        int min = array[0];
        int max = array[0];
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
            if (array[i] < min) {
                min = array[i];
            }
            if (array[i] > max) {
                max = array[i];
            }
        }
        int x = scanner.nextInt();
        int cnt = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < x) {
                cnt++;
            }
        }
        double avg = sum / size;
        System.out.println("avg=" + avg);
        System.out.println("min=" + min);
        System.out.println("max=" + max);
    }
}