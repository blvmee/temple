package lessons.ancient;

import java.util.Scanner;
class third3a{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter size..");
		int size = scanner.nextInt();
		int[] array = new int[size];
		System.out.println("Input numbers:");
		for(int i=0;i<array.length;i++){
			array[i] = scanner.nextInt();
		}
		size--;
		while(size>=0){
			System.out.print(array[size]+", ");
			size--;
		}
		System.out.println();
		System.out.println("----------------");
		int t = 0;
		int p = array.length - 1;
		for(int i = 0; i<array.length/2;i++){
			t = array[i];
			array[i]=array[(p-i)];
			array[(p-i)]=t;	
		}
		for(int i = 0; i<array.length;i++){
			System.out.print(array[i]+", ");
		}
	}
}