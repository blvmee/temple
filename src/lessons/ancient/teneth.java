package lessons.ancient;

import java.util.Scanner;

public class teneth {
    public static void main(String[] args) throws InterruptedException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input string..");
        String x = sc.nextLine();
        System.out.println("Palindrome = " + palindrome(x));
        System.out.println("Uniquecheck = " + uniquecheck(x));
        System.out.println("Sorted string:");
        System.out.println(sort(x));
        System.out.println("Enter long string:");
        String most = sc.nextLine();
        System.out.println("Enter short string:");
        String least = sc.nextLine();
        System.out.println("Long contains short = " + onecontainsanother(most, least));
    }

    public static boolean palindrome(String x) {
        char[] charx = x.toCharArray();
        for (int i = 0; i < x.length() / 2; i++) {
            charx[i] = charx[x.length() - 1 - i];
        }
        String y = new String(charx);
        return (x.equals(y));
    }

    public static boolean uniquecheck(String x) {
        boolean b = true;
        for (int i = 0; i < x.length(); i++) {
            if (x.indexOf(x.charAt(i)) != x.lastIndexOf(x.charAt(i))) {
                b = false;
                break;
            }
        }
        return b;
    }

    public static String sort(String x) {
        String[] splitx = x.split(" ");
        int max = splitx.length;
        for (int i = 0; i < max; i++) {
            for (int j = 1; j < max; j++) {
                while (splitx[j].compareTo(splitx[j - 1]) < 0) {
                    String t = splitx[j];
                    splitx[j] = splitx[j - 1];
                    splitx[j - 1] = t;
                }
            }
        }
        return String.join(" ", splitx);
    }

    public static boolean onecontainsanother(String most, String least) {
        char[] mostarr = most.toCharArray();
        char[] leastarr = least.toCharArray();
        int coincidences = 0;
        int i = 0;
        while ((i < mostarr.length) && (mostarr[i] != leastarr[0])) {
            i++;
        }
        if (i == mostarr.length) {
            return false;
        } else {
            boolean breakIsFound = false;
            int j = 0;
            while ((!breakIsFound) && (coincidences < leastarr.length) && (i < mostarr.length)) {
                if (mostarr[i] != leastarr[j]) {
                    breakIsFound = true;
                } else {
                    coincidences++;
                    i++;
                    j++;
                }
            }
            if (coincidences == leastarr.length) {
                return true;
            } else {
                // обрубаем строку, остаток строки прогоняем снова
                if (i < mostarr.length) {
                    int size = mostarr.length - i;
                    char[] dmost = new char[size];
                    for (int k = 0; k < dmost.length; k++) {
                        dmost[k] = mostarr[i];
                        i++;
                    }
                    String newmost = new String(dmost);
                    return onecontainsanother(newmost, least);
                } else {
                    return false;
                }
            }
        }
    }
}
