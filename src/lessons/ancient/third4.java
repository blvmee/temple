package lessons.ancient;

import java.util.Scanner;
class third4{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter size..");
		int size = scanner.nextInt();
		int[] a = new int[size];
		System.out.println("Input numbers..");
		for(int i = 0;i<a.length;i++){
			a[i]=scanner.nextInt();
		}
		int c=0;
		for(int i=0;i<a.length;i++){
			if(a[i]==0){
				for(int j=i;j>c;j--){
					a[j]=a[j-1];
				}
				a[c]=0;
				c++;				
			}
		}
		for(int i = 0; i<a.length;i++){
			System.out.print(a[i]+", ");
		}
	}
}