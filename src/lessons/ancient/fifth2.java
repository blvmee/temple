package lessons.ancient;

import java.util.Scanner;
public class fifth2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		int b = sc.nextInt();
		int years = 0;
		while(b>=a){
			a*=3;
			b*=2;
			years++;
		}
		System.out.println(years);
	}
}