package lessons.ancient;

import java.util.Scanner;
class fourth1{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter size..");
		int size = scanner.nextInt();
		int[] a = new int[size];
		System.out.println("Input numbers..");
		for(int i = 0;i<a.length;i++){
			a[i]=scanner.nextInt();
		}
		int cnt=0;
		System.out.println("Your array:");
		for(int i = 0;i<a.length;i++){
			System.out.print(a[i]+" ");
		}
		System.out.println();
		for(int c=0;c<a.length;c++){
			boolean isSorted=true;
			for(int i = 0;i<a.length-1;i++){
				if(a[i]>a[i+1]){
					int t = a[i];
					a[i]=a[i+1];
					a[i+1]=t;
					isSorted=false;
				}
			}
			cnt++;
			if(isSorted==true){
				break;
			}
		}
		System.out.println("Sorted array:");
		for(int i = 0;i<a.length;i++){
			System.out.print(a[i]+" ");
		}
		System.out.println();
		System.out.print(cnt+" enterings");
	}
}