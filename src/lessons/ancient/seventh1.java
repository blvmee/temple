package lessons.ancient;

import java.util.Scanner;

class seventh1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter width..");
        int width = sc.nextInt();
        System.out.print("Enter height..");
        int height = sc.nextInt();
        int[] [] matrix = new int[width] [height];
        System.out.println("Input numbers:");
        for(int i = 0; i < height; i++){
            for(int j = 0;j < width;j++){
                matrix[j] [i] = sc.nextInt();
            }
        }
        System.out.println();
        for(int i = 0; i < height; i++){ //рамка
            for(int j = 0;j < width;j++){
                if((i==0|i==height-1)|(j==0|j==width-1)){
                    System.out.print(matrix[j] [i]+" ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
}