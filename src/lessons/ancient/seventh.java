package lessons.ancient;

import java.util.Scanner;

class seventh {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter width..");
        int width = sc.nextInt();
        System.out.print("Enter height..");
        int height = sc.nextInt();
        int[] [] matrix = new int[width] [height];
        System.out.print("Input numbers..");
        for(int i = 0; i < matrix[0].length; i++){
            for(int j = 0;j < matrix.length;j++){
                matrix[i] [j] = sc.nextInt();
            }
        }
        System.out.println();
        for(int i = 0; i < matrix[0].length; i++){
            for(int j = 0;j < matrix.length;j++){
                System.out.print(matrix[i] [j]+" ");
            }
            System.out.println();
        }
    }
}