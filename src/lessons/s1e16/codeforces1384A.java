package lessons.s1e16;

import java.util.Scanner;
import java.util.Random;

public class codeforces1384A {

    static Random rand = new Random();
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int amountOfInputs = scan.nextInt();
        for (int a = 0; a < amountOfInputs; a++) {
            int amountOfElements = scan.nextInt();
            String[] output = new String[amountOfElements + 1];
            output[0] = getRandomWord();
            for (int i = 1; i < output.length; i++) {
                int lcp = scan.nextInt();
                if ((i == 1) && (output[0].length() < lcp)) {
                    for (int b = 0; b < lcp; b++) {
                        output[0] = output[0] + getRandomChar();
                    }
                }
                output[i] = adjustWordRandomly(output[i - 1], lcp);

            }
            for (String s : output) {
                System.out.println(s);
            }
        }
    }

    static char[] letters = "qwertyuiopasdfghjklzxcvbnm".toCharArray();

    static char getRandomChar() {
        return letters[rand.nextInt(letters.length)];
    }

    static String getRandomWord() {
        String result = "";
        // result = result + charCycle();
        int length = 3 + rand.nextInt(4);
        for (int i = 0; i < length; i++) {
            result = result + getRandomChar();
        }
        return result;
    }

    static int cycle = -1;

    static char charCycle() {
        cycle++;
        return letters[cycle % letters.length];
    }

    static String adjustWordRandomly(String previousStr, int prefixLength) {
        if (prefixLength == 0) return getRandomWord();
        String str;
        if (previousStr.length() >= prefixLength) {
            str = previousStr.substring(0, prefixLength);
        } else {
            str = previousStr;
            for (int i = 0; i <= prefixLength; i++) {
                str = str + getRandomChar();
            }
        }
        str = str + charCycle();
        String t = ", base - " + str + ", length - " + prefixLength;
        int amountOfAdditionalCharacters = str.length() + rand.nextInt(4);
        for (int i = 0; i < amountOfAdditionalCharacters; i++) {
            str = str + getRandomChar();
        }
        return str;
    }

}
