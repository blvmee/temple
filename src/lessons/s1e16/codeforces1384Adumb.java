package lessons.s1e16;

import java.util.Scanner;

public class codeforces1384Adumb {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int amountOfInputs = scan.nextInt();
        for (int p = 0; p < amountOfInputs; p++) {
            int amountOfElements = scan.nextInt();
            int[] prefixes = new int[amountOfElements];
            for (int i = 0; i < prefixes.length; i++) {
                prefixes[i] = scan.nextInt();
            }
            // create strings
            String[] output = new String[prefixes.length];
            for (int i = 0; i < output.length; i++) {
                out:
                if (i == 0) {
                    output[0] = "";
                    for (int j = 0; j < prefixes[0]; j++) {
                        output[0] = output[0] + 'a';
                    }
                } else {
                    char change = i % 2 == 0 ? 'a' : 'b';
                    if (prefixes[i] == 0) {
                        output[i] = output[i] + change;
                        break out;
                    }
                    if (output[i-1].length() < prefixes[i]) {
                        char invertedChange = i % 2 == 0 ? 'b' : 'a';
                        while (output[i-1].length() < prefixes[i]) {
                            output[i-1] = output[i-1] + invertedChange;
                        }
                    }
                    output[i] = output[i-1].substring(0,prefixes[i]); // i might not be right
                }

            }
            for (String s: output) {
                System.out.println(s);
            }
        }
    }

}
