package lessons.s1e16;

import java.util.Random;
import java.util.Scanner;

public class ThirdAttempt {

    static Random rand = new Random();
    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        int amountOfInputs = scan.nextInt();
        for (int o = 0; o < amountOfInputs; o++) {
            int amountOfElements = scan.nextInt();
            String[] outputs = new String[amountOfElements];
            outputs[0] = getRandomWord();
            for (int p = 1; p < amountOfElements; p++) {
                int givenPrefixLength = scan.nextInt();
                if (givenPrefixLength > outputs[p-1].length()) {
                    outputs[p-1] = adjustWord(outputs[p-1], givenPrefixLength - outputs[p-1].length());
                } else {
                    outputs[p] = newWordBased(outputs[p - 1], givenPrefixLength);
                }
            }
            for (String out : outputs) {
                System.out.println(out);
            }
        }
    }

    static char[] letters = "qwertyuiopasdfghjklzxcvbnm".toCharArray();

    static int cycle = 0;
    static char charCycled = 'q';

    static char getRandomChar() {
        return letters[rand.nextInt(letters.length)];
    }

    static char charCycle() {
        cycle++;
        charCycled = letters[cycle % letters.length];
        return charCycled;
    }

    static String getRandomWord() {
        String result = "";
        result = result + charCycle();
        int length = 3 + rand.nextInt(4);
        for (int i = 0; i < length; i++) {
            result = result + getRandomChar();
        }
        return result;
    }

    static String adjustWord(String wordToAdjust, int newCharsAmount) {
        StringBuilder result = new StringBuilder(wordToAdjust);
        for (int i = 0; i < newCharsAmount; i++) {
            result.append(getRandomChar());
        }
        return result.toString();
    }

    static String newWordBased(String prevWord, int pref) {
        if (pref == 0) {
            return getRandomWord();
        }
        String result;
        if (pref > prevWord.length()) {
            result = adjustWord(prevWord,pref - prevWord.length());
        } else {
            result = prevWord.substring(pref);
        }
        result = result + charCycle();
        return result;
    }

}
