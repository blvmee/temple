package lessons.threads;

import java.util.Random;

public class Praktikuemsya1 {
    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            int finalI = i + 1;
            Thread thread = new Thread(() -> {
                System.out.println("Starting thread #" + finalI);
                try {
                    Thread.sleep(new Random().nextInt(1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Finished thread #" + finalI);
            });
            thread.start();
            thread.join();
        }
    }
    
}
