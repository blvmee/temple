package lessons.threads;

import java.util.Random;

public class Praktikuemsya2 {
    public static void main(String[] args) throws InterruptedException {

        Thread thread = new Thread(() -> {
            System.out.println("БЕГУ!!!1");
            for (int i = 0; i < 5; i++) {
                System.out.println("Пробежал точку " + (i + 1));
                try {
                    Thread.sleep(new Random().nextInt(4000));
                } catch (InterruptedException e) {
                    System.err.println("не успел");
                    break;
                }
            }
        });
        thread.start();

        Thread.sleep(5000);
        if (thread.isAlive()) {
            thread.interrupt();
        } else {
            System.out.println("успел");
        }
    }
}
