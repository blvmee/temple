package lessons.concurrency;

public class MyRaceCondition {
    public static void main(String[] args) throws InterruptedException {
        boolean testFailed = false;
        for (int i = 0; !testFailed; i++) {
            System.out.print(i + " ");
            var result = new StringBuffer();
            BankAccount account = new BankAccount(500);
            var male = new Thread(() -> {
                result.append(account.doTransaction(500));
            });
            var female = new Thread(() -> {
                result.append(account.doTransaction(500));
            });
            male.start();
            female.start();
            male.join();
            female.join();
//            Thread.sleep(5);
            System.out.print(result + " ");
            if (result.toString().equals("truetrue") && account.balance < 0) {
                testFailed = true;
            }
            System.out.println(account.balance);
            if (i != 0 && i % 1000 == 0) {
                System.gc();
            }
        }
    }
}

class BankAccount {
    public int balance;

    public BankAccount(int balance) {
        this.balance = balance;
    }

    public boolean doTransaction(int cost) {
        if (balance - cost < 0) return false;
        balance -= cost;
        return true;
    }
}