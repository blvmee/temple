package lessons.concurrency;

public class Queue {

    public static void main(String[] args) {
        new Queue().go();
    }

    private void go() {

        MyQ myQ = new MyQ();

        new Consumer(myQ);
        new Producer(myQ);

    }


    static class Producer implements Runnable {

        public MyQ queue;

        public Producer(MyQ queue) {
            this.queue = queue;
            new Thread(this, "Поставщик").start();
        }

        @Override
        public void run() {
            for (int i = 1; i < 5000; i++) {
                queue.put(i);
            }
        }
    }

    static class Consumer implements Runnable {

        public MyQ queue;

        public Consumer(MyQ queue) {
            this.queue = queue;
            Thread consumer = new Thread(this, "Потребитель");
            consumer.setDaemon(true);
            consumer.start();
        }

        @Override
        public void run() {
            while (true) {
                queue.get();
            }
        }
    }

    static class MyQ {
        int n;

        synchronized void put(int n) {
            this.n = n;
            System.out.println("+ Put: " + n);
            notify();
            try {
                wait(1);
            } catch (InterruptedException e) {}
        }

        synchronized void get() {
            try {
                wait();
            } catch (InterruptedException e) {}
            System.out.println("- Get: " + n);
            notify();
        }
    }
}
