package lessons.concurrency;

import java.util.Random;

public class To4ki {

    public static void main(String[] args) throws InterruptedException {
        Runner runner = new Runner();
        runner.start();
        Thread.sleep(5000);
        if (runner.isAlive()) {
            runner.interrupt();
        } else {
            System.out.println("Успел");
        }
    }
}

class Runner extends Thread {

    @Override
    public void run() {
        System.out.println("БЕГУ!!!");
        Random rand = new Random();
        for (int i = 1; i <= 5; i++) {
            try {
                Thread.sleep(rand.nextInt(2000));
                System.out.println(i + "/" + 5);
            } catch (InterruptedException e) {
                System.err.println("Не успел");
                break;
            }
        }
    }
}
