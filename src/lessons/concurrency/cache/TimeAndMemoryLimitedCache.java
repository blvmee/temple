package lessons.concurrency.cache;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class TimeAndMemoryLimitedCache extends MemoryLimitedCache {
    private final ConcurrentHashMap<Integer, Long> timeMap;
    long maxAgeNanos;

    public TimeAndMemoryLimitedCache(int maxEntries, long maxAgeNanos) {
        super(maxEntries);
        this.maxAgeNanos = maxAgeNanos;
        this.timeMap = new ConcurrentHashMap<>();
    }

    @Override
    public boolean has(int key) {
        if (!super.has(key)) {
            System.out.println(Thread.currentThread().getName() + ":\t [TML] Value not found for key " + key);
            return false;
        }
        long currTime = System.nanoTime();
        long valueAge = Optional.of(timeMap.get(key)).orElse(currTime);
        if (currTime - valueAge > maxAgeNanos) {
            System.out.println(Thread.currentThread().getName() + ":\t [TML] Found value, value is too old. Deleting old value..");
            cachedValues.remove(key);
            return false;
        }
        System.out.println(Thread.currentThread().getName() + ":\t [TML] Found value, value is fresh for key " + key);
        return true;
    }

    @Override
    public void add(int key, int value) {
        int prevSize = cachedValues.size();
        super.add(key, value);
        if (prevSize < cachedValues.size()) {
            timeMap.put(key, System.nanoTime());
        }
    }
}
