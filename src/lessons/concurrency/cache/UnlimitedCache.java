package lessons.concurrency.cache;

public class UnlimitedCache extends Cache {

    @Override
    public void add(int key, int value) {
        System.out.println(Thread.currentThread() + ":\t [U] Adding " + value + " for key " + key);
        cachedValues.put(key, value);
    }


}
