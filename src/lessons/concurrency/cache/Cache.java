package lessons.concurrency.cache;

import java.util.concurrent.ConcurrentHashMap;

public abstract class Cache {

    protected ConcurrentHashMap<Integer, Integer> cachedValues = new ConcurrentHashMap<>();

    public abstract void add(int key, int value);

    public boolean has(int arg) {
        return cachedValues.containsKey(arg);
    }

    public int getValue(int key) {
        return this.has(key) ? cachedValues.get(key) : -1;
    }

}
