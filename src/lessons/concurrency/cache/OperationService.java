package lessons.concurrency.cache;

public class OperationService {
    private final Cache cache;
    String prefix;

    public OperationService(Cache cache) {
        this.cache = cache;
        if (cache instanceof TimeAndMemoryLimitedCache) {
            prefix = "[TML]";
        } else if (cache instanceof MemoryLimitedCache) {
            prefix = "[ML]";
        } else {
            prefix = "[U]";
        }
    }

    int performLongAndExpensiveOperation(int key) {
        if (cache.has(key)) {
            return cache.getValue(key);
        } else {
            System.out.println(Thread.currentThread().getName() + ":\t " + prefix + " Cache didnt help for key " + key + ", asking database...");
            return askDatabase(key);
        }
    }

    private int askDatabase(int key) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int result = Double.valueOf(Math.pow(key, 2)).intValue();
        cache.add(key, result);
        return result;

    }
}
