package lessons.concurrency.cache;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        UnlimitedCache unlimitedCache = new UnlimitedCache();
        MemoryLimitedCache memoryLimitedCache = new MemoryLimitedCache(200);
        TimeAndMemoryLimitedCache timeAndMemoryLimitedCache = new TimeAndMemoryLimitedCache(200, 10000000);

        var keys = new LinkedList<Integer>();
        Random random = new Random();
        for (int i = 0; i < 200; i++) {
            keys.add(random.nextInt(500));
        }
        OperationService operationService;

        operationService = new OperationService(unlimitedCache);
        for (int i = 0; i < 15; i++) {
            new Thread(new OperationRunner(keys, operationService)).start();
        }

        operationService = new OperationService(memoryLimitedCache);
        for (int i = 0; i < 15; i++) {
            new Thread(new OperationRunner(keys, operationService)).start();
        }

        operationService = new OperationService(timeAndMemoryLimitedCache);
        for (int i = 0; i < 15; i++) {
            new Thread(new OperationRunner(keys, operationService)).start();
        }

    }

    static class OperationRunner implements Runnable {
        List<Integer> keys;
        OperationService operationService;

        public OperationRunner(List<Integer> keys, OperationService operationService) {
            this.keys = keys;
            this.operationService = operationService;
            Collections.shuffle(keys);
        }

        @Override
        public void run() {
            for (Integer key: keys) {
                operationService.performLongAndExpensiveOperation(key);
            }
        }
    }
}


