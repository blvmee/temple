package lessons.concurrency.cache;

import java.util.Iterator;
import java.util.Random;

public class MemoryLimitedCache extends Cache {
    private final int maxEntries;

    public MemoryLimitedCache(int maxEntries) {
        this.maxEntries = maxEntries;
    }

    @Override
    public int getValue(int key) {
        var result = cachedValues.get(key);
        if (result == null) {
            System.out.println(Thread.currentThread().getName() + ":\t [ML] Value not found");
            return -1;
        }
        return result;
    }

    @Override
    public void add(int key, int value) {
        System.out.print(Thread.currentThread().getName() + ":\t ");
        if (this.has(key)) {
            System.out.println("[ML] Value for key " + key + " already exists, aborting addition...");
            return;
        }
        if (cachedValues.size() == maxEntries) {
            System.out.println("[ML] No space for key " + key + " was available, deleting random value..");
            Iterator<Integer> keysIterator = cachedValues.keys().asIterator();
            int indexOfEntryToDelete = new Random().nextInt(maxEntries);
            int keyOfEntryToDelete = 0;
            for (int i = 0; i < indexOfEntryToDelete; i++) {
                keyOfEntryToDelete = keysIterator.next();
            }
            cachedValues.remove(keyOfEntryToDelete);
        }
        cachedValues.put(key, value);

    }
}
