package lessons.concurrency;

public class TicTac {
    public static void main(String[] args) throws InterruptedException {
        var ticTac = new ConcurrentTicTac();
        Thread tic = new Thread(() -> {
            while (true) {
                ticTac.tic();
            }
        });
        Thread tac = new Thread(() -> {
            while (true) {
                ticTac.tac();
            }
        });
        tic.start();
        tac.start();
    }

    static class ConcurrentTicTac {
        volatile boolean ticTrue_tacFalse = true;

        synchronized void tic() {
            try {
                System.out.println("1 Tic");
                Thread.sleep(1000);
                notify();

                ticTrue_tacFalse = false;
                while (!ticTrue_tacFalse) {
                    wait();
                }
            } catch (InterruptedException e) {}
        }

        synchronized void tac() {
            try {
                while (ticTrue_tacFalse) {
                    wait();
                }

                System.out.println("2 Tac");
                Thread.sleep(1000);

                ticTrue_tacFalse = !ticTrue_tacFalse;
            } catch (InterruptedException e) {}
            notify();
        }
    }
}
