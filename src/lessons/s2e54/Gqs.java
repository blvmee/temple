package lessons.s2e54;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Random;

public class Gqs {

    Queue<String> output;
    public Integer[][] graph;

    public Gqs() {
        output = new ArrayDeque<>();
    }

    public static void main(String[] args) {
        Gqs g = new Gqs();
        g.graph = new Integer[][]{
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0}
        };
        System.out.println(g.breadthSearch(intToBool(g.graph), (new Random().nextInt(g.graph.length))));
    }

    static boolean[][] intToBool(Integer[][] arr) {
        boolean[][] bArr = new boolean[arr.length][arr[0].length];
        for (int i = 0; i < bArr.length; i++) {
            for (int j = 0; j < bArr[0].length; j++) {
                bArr[i][j] = parseBoolean(arr[i][j]);
            }
        }
        return bArr;
    }

    public static <T extends Number> boolean parseBoolean(T arg) {
        return arg.intValue() == 1;
    }

    public String breadthSearch(boolean[][] adjacencyMatrix, int startPoint) {
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[i][startPoint]) System.out.println(i);
        }
        return "";

    }


}