package lessons.s2e54;

import java.util.ArrayDeque;

public class Braces {

    public static void main(String[] args) {
        String[] a = {"as[w(w)d+)", "asdw[dw(das]adw)", "sdw[dw{dfe}adaw]", "[]dwd{wdw(s)}"};
        for (String str : a) {
            System.out.println(bracketsAreCorrectlyPlaced(str));
        }
    }

    static String openingBraces = "([{";
    static String closingBraces = ")]}";

    public static Boolean bracketsAreCorrectlyPlaced(String s) {
        char[] input = s.toCharArray();
        ArrayDeque<Character> stack = new ArrayDeque<>();
        for (Character c : input) {
            if (openingBraces.contains(c.toString())) {
                stack.add(c);
                continue;
            }

            if (!closingBraces.contains(c.toString())) continue;

            if (stack.isEmpty()) return false;

            if (closingBraces.indexOf(c) == openingBraces.indexOf(stack.peekLast())) stack.removeLast();
        }
        return stack.isEmpty();
    }

}
