package lessons.s2e54;

import java.util.Deque;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.ArrayDeque;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean[][] matrix = createMatrix();
        System.out.println("Введите начало отсчёта");
        int start = sc.nextInt();
        System.out.println(doSearch(matrix, start));
    }

    static String doSearch(boolean [][] adjacencyMatrix, int startIndex) {
        Deque<Integer> queue = new ArrayDeque<>();
        StringBuilder output = new StringBuilder();
        int nodeCount = adjacencyMatrix.length;
        boolean[] visitedNodes = new boolean[nodeCount];
        queue.offerFirst(startIndex);
        while (queue.size() != 0) {
            int focus = queue.pollFirst();
            visitedNodes[focus] = true;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (adjacencyMatrix[i][focus] && !visitedNodes[i]) {
                    queue.offerLast(i);
                    visitedNodes[i] = true;
                }
            }
            output.append(focus).append(",");
        }
        return output.substring(0, output.length() - 1);
    }

    static public boolean[][] createMatrix() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число вершин:");
        int a = sc.nextInt();
        int[][] before = new int[a][a];
        System.out.println("Введите матрицу:");
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                before[i][j] = sc.nextInt();
            }
        }
        boolean[][] matrix = new boolean[a][a];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < a; j++) {
                matrix[i][j] = before[i][j] == 1;
            }
        }
        return matrix;
    }

}