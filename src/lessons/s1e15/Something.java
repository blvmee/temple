package lessons.s1e15;

public class Something {

    static int x;

    public Something() {
        x++;
    }

    public static int getCount() {
        return x;
    }
}
