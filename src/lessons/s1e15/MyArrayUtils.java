package lessons.s1e15;

import java.math.BigInteger;

public class MyArrayUtils {

    private MyArrayUtils() {}

    public static <T extends Comparable<T>> void bubbleSort(T[] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 1; j < a.length; j++) {
                while (a[j].compareTo(a[j - 1]) < 0) {
                    T t = a[j];
                    a[j] = a[j - 1];
                    a[j - 1] = t;
                }
            }
        }
    }

    public static <T extends Comparable<T>> void insertionSort(T[] a) {
            for (int i = 1; i < a.length; i++) {
                T key = a[i];
                int j = i - 1;
                while (j >= 0 && a[j].compareTo(key) > 0) {
                    a[j + 1] = a[j];
                    j = j - 1;
                }
                a[j + 1] = key;
            }
    }

    public static BigInteger fact(int f) {
        BigInteger fact = BigInteger.valueOf(1);
        for (int i = 1; i <= f; i++) {
            fact = fact.multiply(BigInteger.valueOf(i));
        }
        return fact;
    }

}
