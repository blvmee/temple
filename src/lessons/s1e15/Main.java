package lessons.s1e15;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        task1();
        Integer[] arrayToSort = {9,8,7,6,5,4,3,2,1};
        MyArrayUtils.bubbleSort(arrayToSort);
        for (int i : arrayToSort) {
            System.out.print(i + " ");
        }
        System.out.println();
        Integer[] array2Sort = {9,8,7,6,5,4,3,2,1};
        MyArrayUtils.insertionSort(array2Sort);
        for (int i : array2Sort) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("factorio");
        BigInteger fak = MyArrayUtils.fact(scan.nextInt());
        System.out.println(fak);
        System.out.println("enter big string arr");

        scan.next();
        String[] star = scan.nextLine().split(" ");
        System.out.println("enter smal strink");
        String ministr = scan.nextLine();
        System.out.println(oneContainsAnother(star,ministr));
    }

    public static void task1() {
        System.out.println("enter many objects (int count)");
        int n = scan.nextInt();
        Something buffer = new Something();
        for (int i = 0; i < n; i++) {
            buffer = new Something();
        }
        System.out.println(Something.getCount());
    }

    static boolean oneContainsAnother(String[] array, String stroka) {
        for (String big : array) {
            if (stroka.contains(big)) return true;
        }
        return false;
    }

}
