package random.racing;

public abstract class Vehicle {

    String name;
    String color;
    int speed;
    int maxSpeed;

    public Vehicle() {
        this.maxSpeed = 200;
    }

    public Vehicle(String name, int speed, int maxSpeed) {
        this.name = name;
        this.speed = speed;
        this.maxSpeed = maxSpeed;
    }

    public Vehicle(String name, String color, int speed, int maxSpeed) {
        this.name = name;
        this.speed = speed;
        this.color = color;
        this.maxSpeed = maxSpeed;
    }

    void accelerate(int acceleration) {
        if (this.speed == maxSpeed) {
            System.out.println("The car can't go any faster!");
            return;
        }
        if (this.speed + acceleration >= maxSpeed) {
            this.speed = maxSpeed;
            System.out.println("The car is now at maximum speed!");
            return;
        }
        this.speed += acceleration;
        System.out.println("The " + this.name + " was accelerated to " + this.speed + " km/h.");
    }

    @Override
    public String toString() {
        return this.name + ", current speed = " + this.speed;
    }

}
