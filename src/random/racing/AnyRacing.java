package random.racing;

import java.util.ArrayList;
import java.util.Scanner;

public class AnyRacing {

    static Scanner scan = new Scanner(System.in);

    static ArrayList<Vehicle> autos = new ArrayList<>();

    public static void main(String[] args) {
        handleAutos();
        System.out.println("Select a car:");
        outputAutos();
        System.out.print("Input index..");
        int index = scan.nextInt();
        System.out.println();
        System.out.println("Your action?");
        outputControls();
        int action = scan.nextInt();
        controls(action, autos.get(index));
    }

    static void handleAutos(){
        autos.add(new Car());
        autos.add(new Supercar( "Ferrari F40","Red",140,220));
        autos.add(new Supercar( "McLaren P1","Yellow",160,245));
        autos.add(new Supercar("Koenigsegg One:1",180,260));
        autos.add(new Motorcycle("Kawasaki Ninja",110,170));
        autos.add(new Motorcycle("URAL","Shrek",70,130));
        // autos.add(new Vehicle("Robot Vacuum Cleaner",5,10));
    }

    static void outputAutos(){
        System.out.println("---------------------------------");
        for (int i = 0; i < autos.size(); i++) {
            System.out.println("[" + i + "] " + autos.get(i).toString());
        }
        System.out.println("---------------------------------");
    }

    static String[] controls = {"Accelerate", "Auto info"};

    static void outputControls() {
        for (int i = 0; i < controls.length; i++) {
            System.out.println(i + " - " + controls[i]);
        }
    }

    static void controls(int input,Vehicle auto) {
        switch (input) {
            case 0:
                System.out.println("You are about to accelerate " + auto.name + ", which is currently racing at " + auto.speed + " km/h.");
                auto.accelerate(scan.nextInt());
                break;
            case 1:
                System.out.println(auto.toString());
                break;
        }
    }

}
