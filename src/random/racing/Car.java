package random.racing;

public class Car extends Vehicle {

    public Car() {
        super();
        this.name = "Hyundai Solaris";
        this.speed = 65;
        this.color = "White";
    }

    public Car(String name, int speed, int maxSpeed) {
        super(name, speed, maxSpeed);
    }

    public Car(String name,  String color,int speed,int maxSpeed) {
        super(name, color, speed, maxSpeed);
    }

}
