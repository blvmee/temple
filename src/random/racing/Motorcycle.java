package random.racing;

public class Motorcycle extends Vehicle {

    public Motorcycle(String name, int speed, int maxSpeed) {
        super(name, speed, maxSpeed);
    }

    public Motorcycle(String name, String color, int speed, int maxSpeed) {
        super(name, color, speed, maxSpeed);
    }

}
