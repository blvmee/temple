package random.racing;

public class Supercar extends Car {

    String someFieldToDiffer;

    public Supercar(String name, int speed ,int maxSpeed) {
        super(name,speed,maxSpeed);
    }

    public Supercar(String name, String color,int speed,int maxSpeed) {
        super(name, color, speed, maxSpeed);
    }

}
