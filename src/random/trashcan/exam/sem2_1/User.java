package random.trashcan.exam.sem2_1;

public class User {
    private final Long id;
    private final String name;
    private final String email;
    private final String city;
    private final Integer birthYear;

    public static final User NULL_USER = new User(0L, "", "", "", 0);

    public User(Long id, String name, String email, String city, Integer birthYear) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.city = city;
        this.birthYear = birthYear;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getCity() {
        return city;
    }

    public Integer getBirthYear() {
        return birthYear;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", city='" + city + '\'' +
                ", birthYear=" + birthYear +
                '}';
    }
}
