package random.trashcan.exam.sem2_1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Parser {
    private final String USERS_SOURCE = "src/random/trashcan/exam/sem2/data/Users.tsv";

    public List<User> parseUsers() {
        List<User> result = new LinkedList<>();
        List<String> fileStrings = getFileStrings(USERS_SOURCE);
        fileStrings.forEach(s -> {
            String[] args = s.split("\t");
            var id = Long.parseLong(args[0]);
            var name = args[1];
            var email = args[2];
            var city = args[3];
            var birthYear = Integer.parseInt(args[4]);
            result.add(new User(id, name, email, city, birthYear));
        });
        return result;
    }

    private final String MESSAGES_SOURCE = "src/random/trashcan/exam/sem2/data/Messages.tsv";

    public List<Message> parseMessages() {
        List<Message> result = new LinkedList<>();
        List<String> fileStrings = getFileStrings(MESSAGES_SOURCE);
        fileStrings.forEach(s -> {
            String[] args = s.split("\t");
            var id = Long.parseLong(args[0]);
            var text = args[1];
            var authorId = Long.parseLong(args[2]);
            result.add(new Message(id, text, authorId));
        });
        return result;
    }

    private final String LIKES_SOURCE = "src/random/trashcan/exam/sem2/data/Likes.tsv";

    public List<Like> parseLikes() {
        List<Like> result = new LinkedList<>();
        List<String> fileStrings = getFileStrings(LIKES_SOURCE);
        fileStrings.forEach(s -> {
            String[] args = s.split("\t");
            var likeOriginUserId = Long.parseLong(args[0]);
            var messageId = Long.parseLong(args[1]);
            result.add(new Like(likeOriginUserId, messageId));
        });
        return result;
    }

    private List<String> getFileStrings(String path) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File(path)));
            return bufferedReader.lines().collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
