package random.trashcan.exam.sem2_1;

public class Like {
    private final Long likeOriginUserId;
    private final Long messageId;

    public Like(Long likeOriginUserId, Long messageId) {
        this.likeOriginUserId = likeOriginUserId;
        this.messageId = messageId;
    }

    public Long getLikeOriginUserId() {
        return likeOriginUserId;
    }

    public Long getMessageId() {
        return messageId;
    }

    @Override
    public String toString() {
        return "Like{" +
                "likeOriginUserId=" + likeOriginUserId +
                ", messageId=" + messageId +
                '}';
    }
}
