package random.trashcan.exam.sem2_1;

public class Message {
    private final Long id;
    private final String text;
    private final Long authorId;

    public static final Message NULL_MESSAGE = new Message(0L, "", 0L);

    public Message(Long id, String text, Long authorId) {
        this.id = id;
        this.text = text;
        this.authorId = authorId;
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Long getAuthorId() {
        return authorId;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", authorId=" + authorId +
                '}';
    }
}
