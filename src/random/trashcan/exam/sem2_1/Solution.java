package random.trashcan.exam.sem2_1;

import java.util.*;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) {
        try {
            new Solution().run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<User> users;
    private List<Message> messages;
    private List<Like> likes;

    private void run() {
        Parser parser = new Parser();
        users = parser.parseUsers();
        messages = parser.parseMessages();
        likes = parser.parseLikes();

        System.out.println(taskA());
        System.out.println(taskB());
        System.out.println(taskC());
    }

    /*
        Для каждого юзера, у которого email на gmail.com , вывести общее кол-во лайков сообщениям, для которых этот юзер - автор.
        Решить с помощью Stream API
     */
    private Map<User, Long> taskA() {
        return users.stream()
                .filter(user -> user.getEmail().equals("gmail.com"))
                .collect(Collectors.toMap(
                        user -> user,
                        user -> messages.stream()
                            .filter(message -> message.getAuthorId().equals(user.getId()))
                            .map(message -> likes.stream()
                                    .filter(like -> like.getMessageId().equals(message.getId()))
                                    .count()
                            )
                            .reduce(Long::sum)
                            .orElse(0L)));
    }

    /*
        Вывести юзеров, которые ставили лайки только сообщениям юзеров из своего города
        Решить с помощью обработки коллекций циклами
     */
    private List<User> taskB() {
        List<User> result = new ArrayList<>();
        out:
        for (User user : users) {
            boolean flag = false;
            for (Like like : likes) {
                if (like.getLikeOriginUserId().equals(user.getId())) {
                    Long messageId = like.getMessageId();
                    for (Message message : messages) {
                        if (message.getId().equals(messageId)) {
                            Long messageAuthorId = message.getAuthorId();
                            for (User user1: users) {
                                if (user1.getId().equals(messageAuthorId)) {
                                    if (!user1.getCity().equals(user.getCity())) {
                                        continue out;
                                    }
                                }
                            }
                        }
                    }
                    flag = true;
                }
            }
            if (flag) result.add(user);
        }
        return result;
    }

    /*
        Проверить, что существуют пользователи, которые поставили свои лайки людям, которые младше их
     */
    private Boolean taskC() {
        return users.stream()
                .anyMatch(user -> likes.stream()
                        .filter(like -> like.getLikeOriginUserId().equals(user.getId()))
                        .allMatch(like -> {
                            Integer userBirthYear = user.getBirthYear();
                            Message message = Optional.ofNullable(getMessageById(like.getMessageId())).orElse(Message.NULL_MESSAGE);
                            User author = Optional.ofNullable(getUserById(message.getAuthorId())).orElse(User.NULL_USER);
                            return userBirthYear < author.getBirthYear();
                        }));
    }

    private User getUserById(Long id) {
        for (User user : users) {
            if (user.getId().equals(id)) return user;
        }
        return null;
    }

    private Message getMessageById(Long id) {
        for (Message message : messages) {
            if (message.getId().equals(id)) return message;
        }
        return null;
    }

    // Task D
    static class UserComparator implements Comparator<User> {

        private List<User> users;
        private List<Like> likes;

        @Override
        public int compare(User user1, User user2) {
            Integer likesLeftByUser1 = 0;
            Integer likesLeftByUser2 = 0;
            // TODO
            return likesLeftByUser1 - likesLeftByUser2;
        }
    }

}
