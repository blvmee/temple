package random.trashcan.exam.sem2_th;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Anno {
    public static void main(String[] args) {

    }

    public List<Bean> parse(String filename) throws FileNotFoundException {
        ArrayList<Bean> res = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        reader.lines()
                .forEach(line -> {
                    String[] args = line.split("\t");
                    Function<Integer, Field> findFieldByColumn = (i) -> {
                        Field[] declaredFields = Bean.class.getDeclaredFields();
                        for (Field f : declaredFields) {
                            Column column = f.getAnnotation(Column.class);
                            if (i.equals(column.position())) return f;
                        }
                        return null;
                    };
                });
        return res;
    }

}

class Bean {

}

@interface Column {
    int position();
}
