package random.trashcan.exam.sem2_th;

import java.io.BufferedReader;

public class Deadlock {
    private final Object left = new Object();
    private final Object right = new Object();

    void leftRight() {
        try {
            synchronized (left) {
                System.out.println(Thread.currentThread().getName() + " holding left");
                Thread.sleep(2000);
                synchronized (right) {
                    System.out.println(Thread.currentThread().getName() + " holding right");
                    System.out.println("Blabla");
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InterruptedException e) {}
    }

    void rightLeft() {
        try {
            synchronized (right) {
                System.out.println(Thread.currentThread().getName() + " holding right");
                Thread.sleep(2000);
                synchronized (left) {
                    System.out.println("Haha");
                    System.out.println(Thread.currentThread().getName() + " holding left");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InterruptedException e) {}
    }

    public static void main(String[] args) {
        Deadlock lock = new Deadlock();
        Thread thread1 = new Thread(lock::leftRight);
        Thread thread2 = new Thread(lock::rightLeft);
        thread1.start();
        thread2.start();
    }
}