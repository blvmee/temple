package random.trashcan.exam.sem2_th;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ThFirst {
    // с помощью рефлексии вызвать метод у класса у которого нет параметров, написать код, название класса и метода даются параметром
    public static void main(String[] args) throws ReflectiveOperationException {
        new ThFirst().callMethodWithoutArgsOnClass("random.trashcan.exam.sem2_th.ThTestOne", "method");
        new ThFirst().callMethodWithoutArgsOnClass("random.trashcan.exam.sem2_th.ThTestTwo", "method");
    }

    public void callMethodWithoutArgsOnClass(String className, String methodName) throws ReflectiveOperationException {
        Class<?> clazz = Class.forName(className);
        Method methodToCall = clazz.getDeclaredMethod(methodName);
        Object target = null;
        if (!Modifier.isStatic(methodToCall.getModifiers())) {
            Constructor<?> declaredConstructor = clazz.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            target = declaredConstructor.newInstance();
        }
        methodToCall.invoke(target);
    }
}

class ThTestOne {
    public void method() {
        System.out.println("Hi 1");
    }
}

class ThTestTwo {
    public static void method() {
        System.out.println("Hi 2");
    }
}



