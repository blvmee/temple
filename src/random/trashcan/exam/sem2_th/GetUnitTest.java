package random.trashcan.exam.sem2_th;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class GetUnitTest {

    static ArrayList<String> list = new ArrayList<>();

    @Test
    public void testGetMethod() {
        list.add("string");
        Assertions.assertEquals(list.get(0), "string");
    }

}
