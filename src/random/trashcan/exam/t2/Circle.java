package random.trashcan.exam.t2;

public class Circle {

    private int x;
    private int y;
    private int radius;
    double square;
    double length;
    double pi = Math.PI;

    public Circle(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        calculateSquare();
        calculateLength();
    }

    void calculateSquare() {
        square = pi * pi * radius;
    }

    void calculateLength() {
        length = 2 * pi * radius;
    }

    boolean intersectsWithAnotherCircle(Circle otherCircle) {
        if ((this.x == otherCircle.getX()) && (this.y == otherCircle.getY()))
        {
            return true;
        }
        if (this.x == otherCircle.getX())
        {
            return Math.abs(this.y - otherCircle.getY()) <= this.radius + otherCircle.getRadius();
        }
        if (this.y == otherCircle.getY())
        {
            return Math.abs(this.x - otherCircle.getX()) <= this.radius + otherCircle.getRadius();
        }
        Triangle t = new Triangle(this.x, this.y, this.y, otherCircle.getY());
        return t.getSideC() < this.radius + otherCircle.getRadius();
    }



    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getRadius() {
        return radius;
    }

    public double getSquare() {
        return square;
    }

    public double getLength() {
        return length;
    }
}
