package random.trashcan.exam.t2;

public class Triangle {
    
    private final int sideA;
    private final int sideB;
    private final double sideC;

    public Triangle(int xCoordA, int xCoordB, int yCoordB, int yCoordC) {
        sideA = Math.abs(xCoordA - xCoordB);
        sideB = Math.abs(yCoordB - yCoordC);
        sideC = pythagoreanTheorem();
    }

    public double pythagoreanTheorem() {
        return Math.sqrt((double) sideA * sideA + sideB * sideB);
    }

    public double getSideC() {
        return sideC;
    }
}
