package random.trashcan.exam.t2;

public class Main {

    public static void main(String[] args) {
        task2();
        int s = task4(0,0);
        System.out.println(s);
        Circle c1 = new Circle(1,1,5);
        Circle c2 = new Circle(5,5,5);
        System.out.println(c1.intersectsWithAnotherCircle(c2));
    }

    static void task2() {
        /*
        int s = 0;
        int i = 0;
        while (s < 100) {
            s += i++;
        }
        Переписать с for
         */
        int s = 0;
        for (int i = 0; s < 100; i++){
            s += i;
        }
        System.out.println(s);
    }

    static int task4(int s, int i) {
        if (s > 100) {
            return s;
        } else {
            return task4(s + i, i + 1);
        }
    }

    // task 6 - aggregation
    class District {
        House[] houses;
        Road[] roads;
        Park[] parks;
    }

    class House {}
    class Road {}
    class Park {}

}
