package random.trashcan.exam.sem2_ext;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    static List<User> users = parseUsers();
    static List<Item> items = parseItems();
    static List<Favorite> favs = parseFavorits(users, items);

    public static void main(String[] args) {
//        first(users, favs, "Novgorod");
//	      second(users, favs);
//        System.out.println(third(favs, items));
        users.sort(new ComparatorOfUsers());
        users.forEach(System.out::println);
    }

    public static List<User> parseUsers() {
        List<User> result = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("C:/Users/Башир/IdeaProjects/MyLittleOrder/userlist.txt"));
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] createdUser = line.split(" ");
                result.add(new User(Integer.parseInt(createdUser[0]), createdUser[1], Integer.parseInt(createdUser[2]), createdUser[3]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<Item> parseItems() {
        List<Item> result = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("C:/Users/Башир/IdeaProjects/MyLittleOrder/items.txt"));
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] createdItem = line.split(" ");
                if (createdItem[2].charAt(0) > 64 && createdItem[2].charAt(0) < 91) {
                    result.add(new Item(Integer.parseInt(createdItem[0]), createdItem[1], "", createdItem[2]));
                } else result.add(new Item(Integer.parseInt(createdItem[0]), createdItem[1], createdItem[2], createdItem[3]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<Favorite> parseFavorits(List<User> users, List<Item> items) {
        List<Favorite> result = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader("C:/Users/Башир/IdeaProjects/MyLittleOrder/favourites.txt"));
            String line = "";
            while ((line = reader.readLine()) != null) {
                String[] createFav = line.split(" ");
                result.add(new Favorite(users.stream()
                        .filter(user -> user.getId() == Integer.parseInt(createFav[0]))
                        .findFirst().get(), items.stream()
                        .filter(item -> item.getId() == Integer.parseInt(createFav[1]))
                        .findFirst().get()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void first(List<User> users, List<Favorite> favs, String needCity) {
        users.stream()
                .filter(user -> user.getCity().equals(needCity))
                .forEach(us -> {
                    if (favs.stream().anyMatch(fav -> fav.getLover() == us)) {
                        System.out.println(us.getName() + " - " + us.getAmmountOfOrders(favs));
                    }
                });
    }

    public static void second(List<User> users, List<Favorite> favs) {

    }

    public static boolean third(List<Favorite> favs, List<Item> items) {
        Set<String> allDevelopers = new HashSet<>();
        items.forEach(item -> { if (!item.getCreater().equals("")) allDevelopers.add(item.getCreater()); });

        List<Favorite> rightFavs = favs
                .stream()
                .filter(fav -> fav.getLover()
                        .getBirth() > 2003)
                .collect(Collectors.toList());

        Map<User, String> checkedUsers = new HashMap<>();
        for (Favorite fav: rightFavs) checkedUsers.put(fav.getLover(), "");
        for (String dev: allDevelopers) {
            for (Favorite fav: rightFavs) {
                String check = checkedUsers.get(fav.getLover());
                if (fav.getItem().getCreater().equals(dev)) {
                    checkedUsers.put(fav.getLover(), dev);
                    if (!check.equals(checkedUsers.get(fav.getLover())) && !check.equals(""))
                        return false;
                }
            }
        }
        return true;
    }

    static class ComparatorOfUsers implements Comparator<User> {

        @Override
        public int compare(User o1, User o2) {
            return o1.getAmmountOfOrders(favs) > o2.getAmmountOfOrders(favs) ? 0 : -1;
        }
    }
}
