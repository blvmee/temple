package random.trashcan.exam.sem2_ext;

public class Item {

    private int id;
    private String name;
    private String creater;
    private String city;

    public Item(int id, String name, String creater, String city) {
        this.id = id;
        this.name = name;
        this.creater = creater;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", creater='" + creater + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
