package random.trashcan.exam.sem2_ext;

import java.util.List;

public class User {

    private int id;
    private String name;
    private int birth;
    private String city;

    public User(int id, String name, int birth, String city) {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getBirth() {
        return birth;
    }

    public void setBirth(int birth) {
        this.birth = birth;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birth=" + birth +
                ", city='" + city + '\'' +
                '}';
    }

    public int getAmmountOfOrders(List<Favorite> favs) {
        return (int) favs.stream()
                .filter(fav -> fav.getLover() == this).count();
    }
}
