package random.trashcan.exam.sem2_ext;

public class Favorite {

    private User lover;
    private Item item;

    public Favorite(User lover, Item item) {
        this.lover = lover;
        this.item = item;
    }

    public User getLover() {
        return lover;
    }

    public void setLover(User lover) {
        this.lover = lover;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "Favorite{" +
                "lover=" + lover +
                ", item=" + item +
                '}';
    }
}
