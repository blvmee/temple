package random.trashcan.exam.sem2_2;

public class Director {
    private Long id;
    private String name;

    public static Director NULL_DIRECTOR = new Director(0L, "");

    public Director(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}

