package random.trashcan.exam.sem2_2;

import java.io.BufferedReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Solution{
    public static void main(String[] args) {
        new Solution().run();
    }

    private List<Actor> actors;
    private List<Director> directors;
    private List<Movie> movies;
    private Map<Movie, Actor> movieActorMap;

    public Solution() {
        Parser parser = new Parser();
        this.actors = parser.parseActors();
        this.directors = parser.parseDirectors();
        this.movies = parser.parseMovies();
        movieActorMap = fillParticipationMap();
    }

    private void run() {

    }

    private void taskA() {
        class Pair {
            Director director;
            Actor actor;

            public Pair(Director director, Actor actor) {
                this.director = director;
                this.actor = actor;
            }

            @Override
            public String toString() {
                return "Pair{" +
                        "director=" + director +
                        ", actor=" + actor +
                        '}';
            }
        }
    }

    private void taskB() {

    }

    private Director findDirectorById(Long id) {
        for (Director d : directors) {
            if (d.getId().equals(id)) return d;
        }
        return Director.NULL_DIRECTOR;
    }

    private Movie findMovieById(Long id) {
        for (Movie m : movies) {
            if (m.getId().equals(id)) return m;
        }
        return Movie.NULL_MOVIE;
    }

    private Actor findActorById(Long id) {
        for (Actor a : actors) {
            if (a.getId().equals(id)) return a;
        }
        return Actor.NULL_ACTOR;
    }

    public final String MOVIE_PARTICIPATION_PATH = "src/random/trashcan/exam/sem2_2/data/MovieParticipation.tsv";

    private Map<Movie, Actor> fillParticipationMap() {
        List<String> strings = new Parser().parseLines(MOVIE_PARTICIPATION_PATH);
        Map<Movie, Actor> result = new HashMap<>();
        strings.forEach(s -> {
            String[] split = s.split("\t");
            result.put(findMovieById(Long.parseLong(split[0])), findActorById(Long.parseLong(split[1])));
        });
        return result;
    }

}
