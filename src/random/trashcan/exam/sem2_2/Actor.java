package random.trashcan.exam.sem2_2;

public class Actor {
    private Long id;
    private String name;
    private Integer yearsOfExperience;

    public static Actor NULL_ACTOR = new Actor(0L, "", 0);

    public Actor(Long id, String name, Integer yearsOfExperience) {
        this.id = id;
        this.name = name;
        this.yearsOfExperience = yearsOfExperience;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getYearsOfExperience() {
        return yearsOfExperience;
    }
}
