package random.trashcan.exam.sem2_2;

public class Movie {
    private Long id;
    private String name;
    private Integer year;
    private Long directorId;

    public static Movie NULL_MOVIE = new Movie(0L, "", 0, 0L);


    public Movie(Long id, String name, Integer year, Long directorId) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.directorId = directorId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getYear() {
        return year;
    }

    public Long getDirectorId() {
        return directorId;
    }
}
