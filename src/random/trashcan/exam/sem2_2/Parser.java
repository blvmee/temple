package random.trashcan.exam.sem2_2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Parser {
    private final String ACTORS_PATH = "src/random/trashcan/exam/sem2_2/data/Actors.tsv";

    public List<Actor> parseActors() {
        return parseLines(ACTORS_PATH).stream()
                .map(s -> {
                    String[] args = s.split("\t");
                    var id = Long.parseLong(args[0]);
                    var name = args[1];
                    var exp = Integer.parseInt(args[2]);
                    return new Actor(id, name, exp);
                }).collect(Collectors.toList());
    }

    private final String DIRECTORS_PATH = "src/random/trashcan/exam/sem2_2/data/DIRECTORS.tsv";

    public List<Director> parseDirectors() {
        return parseLines(DIRECTORS_PATH).stream()
                .map(s -> {
                    String[] args = s.split("\t");
                    var id = Long.parseLong(args[0]);
                    var name = args[1];
                    return new Director(id, name);
                }).collect(Collectors.toList());
    }

    private final String MOVIES_PATH = "src/random/trashcan/exam/sem2_2/data/MOVIES.tsv";

    public List<Movie> parseMovies() {
        return parseLines(MOVIES_PATH).stream()
                .map(s -> {
                    String[] args = s.split("\t");
                    var id = Long.parseLong(args[0]);
                    var name = args[1];
                    var year = Integer.parseInt(args[2]);
                    var directorId = Long.parseLong(args[3]);
                    return new Movie(id, name, year, directorId);
                }).collect(Collectors.toList());
    }

    public List<String> parseLines(String path) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));
            return reader.lines().collect(Collectors.toList());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}
