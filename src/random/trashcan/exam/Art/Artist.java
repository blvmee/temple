package random.trashcan.exam.Art;

public abstract class Artist {

    String occupation;
    int salary;

    abstract void perform();

    void getPaid(int amount) {
        salary += amount;
    }

    public Artist(String occupation, int salary) {
        this.occupation = occupation;
        this.salary = salary;
    }

}
