package random.trashcan.exam.Art;

public class Musician extends Artist implements ITour {

    String genre;
    String roleInBand;

    public Musician(String genre) {
        super("Music", 300);
        this.genre = genre;
    }

    public Musician(String genre, String roleInBand) {
        this("Rock");
        this.roleInBand = roleInBand;
    }

    @Override
    public void perform() {
        System.out.println("la la la");
    }

    public void goOnTour() {
        getPaid(500000);
    }

}
