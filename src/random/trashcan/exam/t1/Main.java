package random.trashcan.exam.t1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        task3();
    }

    static void task2() {
        /* Переписать через while.
        int s = 1;
        for (int i = 0; i < 10; i++) {
            s += i;
            i++;
        }
        */
        int s = 1;
        int i = 0;
        while (i < 10) {
            s += i;
            i += 2;
        }
    }

    static void task3() {
        // бинарный поиск
        int[] arr = new int[100];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }
        boolean isFound = false;
        int element = new Scanner(System.in).nextInt();
        int i = 0;
        int index = arr.length/2;
        while (!isFound) {
            i++;
            if (arr[index] == element) {
                System.out.println("element found at " + i + " iteration.");
                isFound = true;
            } else {
                index = arr[index] > element ? index - index/2 + 1 : index + index/2 - 1;
            }
        }
    }

    static int task4() {
        /* Переписать через рекурсию.
        int s = 1;
        for (int i = 0; i < 10; i++) {
            s += i;
            i++;
        }
        не ебу как
        */
        return 0;
    }

    static boolean task7(String[] a) {
        String vowels = "eyuioa";
        for (int i = 0; i < a.length - 1; i++) {
            String str1 = a[i];
            String str2 = a[i];
            for (int j = 0; j < str1.length() && j < str2.length() ; j += 2) {
                char c1 = str1.charAt(j);
                char c2 = str2.charAt(j);
                if (Character.isLowerCase(c1) == Character.isLowerCase(c2)) {
                    if (((vowels.indexOf(c1) == -1) & (vowels.indexOf(c2) == -1)) || ((vowels.indexOf(c1) > 0) & (vowels.indexOf(c2) > 0))) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

}

/* violating 1st SOLID principle 6

class Secretary {

    void washDishes(){}
    Report[] doReports(){}
    void cleanFloors(){}
    Meal[] cookDinner(){}
    void lockOffice(){}

}
 */