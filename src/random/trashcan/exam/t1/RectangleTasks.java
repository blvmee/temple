package random.trashcan.exam.t1;

import java.util.ArrayList;
import java.util.Random;

public class RectangleTasks {

    public static void main(String[] args) {
        Random rand = new Random();
        ArrayList<Rectangle> arr = new ArrayList<>();
        int span = 100;
        for (int i = 0; i < 5; i++) {
            arr.add(new Rectangle(rand.nextInt(span), rand.nextInt(span),rand.nextInt(span),rand.nextInt(span)));
        }
        System.out.println(getSumOfSquares(arr));
        System.out.println(getSumOfPerimeters(arr));
        System.out.println(arr.get(rand.nextInt(arr.size())).toString());
        arr.add(new Rectangle(0,1,0,1));
        for (Rectangle rect : arr) {
            System.out.println(rect.toString());
        }
        System.out.println(isEveryRectangleCrossedWithAnother(arr));
    }

    {
        System.out.println("piska");
    }

    static int getSumOfSquares(ArrayList<Rectangle> list) {
        int sum = 0;
        for (Rectangle rect : list) {
            sum += rect.getSquare();
        }
        return sum;
    }

    static int getSumOfPerimeters(ArrayList<Rectangle> list) {
        int sum = 0;
        for (Rectangle rect : list) {
            sum += rect.getPerimeter();
        }
        return sum;
    }

    static boolean isEveryRectangleCrossedWithAnother(ArrayList<Rectangle> list){
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = i + 1; j < list.size(); j++) {
                if (!list.get(i).isCrossedByOtherRectangle(list.get(j))) {
                    return false;
                }
            }
        }
        return true;
    }

}
