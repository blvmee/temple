package random.trashcan.exam.t1;

public class Rectangle {

    public int x1;
    public int x2;
    public int y1;
    public int y2;
    int width;
    int height;
    int square;
    int perimeter;

    public Rectangle(int x1, int x2, int y1, int y2) {
        this.x1 = Math.min(x1,x2);
        this.x2 = Math.max(x1,x2);
        this.y1 = Math.min(y1,y2);
        this.y2 = Math.max(y1,y2);
        height = Math.abs(Math.abs(y1) - Math.abs(y2));
        width = Math.abs(Math.abs(x1) - Math.abs(x2));
        if ((x1 == x2) || (y1 == y2)) {
            square = 0;
            perimeter = 0;
        } else {
            square = width * height;
            perimeter = 2 * width + 2 * height;
        }
    }

    public int getSquare() {
        return square;
    }

    public int getPerimeter() {
        return perimeter;
    }

    public boolean isCrossedByOtherRectangle(Rectangle otherRectangle) {
        Rectangle daddy = new Rectangle(Math.min(this.x1, otherRectangle.x1),
                                        Math.max(this.x2, otherRectangle.x2),
                                        Math.min(this.y1, otherRectangle.y1),
                                        Math.min(this.y2, otherRectangle.y2));
        return daddy.getSquare() < 2 * (this.getSquare() + otherRectangle.getSquare());
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x1 = " + x1 +
                ", x2 = " + x2 +
                ", y1 = " + y1 +
                ", y2 = " + y2 +
                ", width = " + width +
                ", height = " + height +
                ", square = " + square +
                ", perimeter = " + perimeter +
                '}';
    }



}
