package project;

import java.io.*;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Main {
    private static final String PATH = "src/random/project/data/text.txt";

    public static void main(String[] args) {
        WordsHandler wordsHandler = new WordsHandler();
        wordsHandler.parseFile(PATH);

        wordsHandler.output(500);
    }

    static class WordsHandler {
        ArrayList<Word> words = new ArrayList<>();
        String base;

        public void output(int quantity) {
            String word = "";
            for (int i = 0; i < quantity && word != null; i++) {
                word = getNext();
                System.out.print(getNext() + " ");
                if (i % 10 == 0) {
                    System.out.println();
                }
            }
        }

        void parseFile(String path) {
            try (BufferedReader buf = new BufferedReader(new FileReader(path))) {
                String line;
                boolean firstTime = true;
                while ((line = buf.readLine()) != null) {
                    String[] words = line.split(" ");
                    for (int i = 0; i < words.length; i++) {
                        if (firstTime) {
                            addFirstBase(words[i]);
                            firstTime = false;
                        } else {
                            if (!words[i].contains("[") && !words[i].contains("]") )
                                addWord(words[i]);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void addFirstBase(String word) {
            base = word;
        }


        void addWord(String next) { // new base word
            getBaseWord(base).addNext(next);
            words.add(new Word(next));
            base = next;
        }

        Word getBaseWord(String word) {
            for (Word w : words) {
                if (w.getText().equals(word)) {
                    return w;
                }
            }

            words.add(new Word(word));
            return getBaseWord(word);
        }

        boolean isFirst = true;

        void setFirstWord() {
            base = words.get(new Random().nextInt(words.size() / 10)).getText();
        }

        String getNext() {
            if (isFirst) {
                setFirstWord();
                isFirst = false;
            }
            Word next = getBaseWord(base).getNext();
            base = next.getText();
            return next.getText();
        }

        @Override
        public String toString() {
            return "WordsHandler{" +
                    "words=" + words +
                    ", base='" + base + '\'' +
                    '}';
        }

        static class Word {
            ArrayList<Word> next = new ArrayList<>();
            String text;
            int count;

            public Word(String text) {
                this.text = text;
            }

            public void addNext(String word) {
                for (Word w : next) {
                    if (w.getText().equals(word)) {
                        w.incrementCounter();
                        return;
                    }
                }

                next.add(new Word(word));
                addNext(word);
            }

            Word getNext() {
                int total = 0;
                for (Word w : next) {
                    total += w.getCount();
                }
                //System.out.println(this);
                //System.out.println(next);

                int result = new Random().nextInt(total + 1);
                //System.out.println("r " + result + " t " + total);

                for (Word w : next) {
                    result -= w.getCount();
                    if (result <= 0) {
                        //System.out.println(w);
                        return w;
                    }
                }
                return null;
            }

            void incrementCounter() {
                count++;
            }

            public String getText() {
                return text;
            }

            public int getCount() {
                return count;
            }

            @Override
            public String toString() {
                return "\n\t\tWord{" +
                        "next=" + next +
                        ", text='" + text + '\'' +
                        ", count=" + count +
                        '}';
            }
        }
    }
}
