package random.trashcan;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;

public class INeedSomePracticeWith2dArrays {

    public static void main(String[] args) {
        int[][] matrix1 = parseFromTxt("" +
                "1 0 2\n" +
                "0 1 3\n" +
                "2 2 2");
        int[][] matrix2 = parseFromTxt("" +
                "1 3 6\n" +
                "7 1 0\n" +
                "2 3 2");
        int[][] matrix3 = parseFromTxt("" +
                "1 20 3\n" +
                "7 0 2\n" +
                "2 2 2");
        int[][] matrix4 = parseFromTxt("" +
                "1 2 2 1\n" +
                "7 0 14 1\n" +
                "2 4 5 1\n" +
                "9 8 7 6");
        int[][] matrix5 = parseFromTxt("" +
                "0 1 0 0 0\n" +
                "0 0 0 1 0\n" +
                "0 1 0 0 0\n" +
                "1 0 1 0 0\n" +
                "0 1 1 1 0");
        print(product(matrix5, matrix5, matrix5));
    }

    static int[][] sum(int[][]... matrices) {
        int[][] result = new int[matrices[0].length][matrices[0][0].length];
        for (int[][] matrix : matrices) {
            for (int i = 0; i < matrices[0].length; i++) {
                for (int j = 0; j < matrices[0][0].length; j++) {
                    result[i][j] += matrix[i][j];
                }
            }
        }
        return result;
    }

    static int[][] product(int[][]... matrices) {
        for (int i = 1; i < matrices.length; i++) {
            matrices[0] = product(matrices[0], matrices[i]);
        }
        return matrices[0];
    }

    static int[][] product(int[][] matrix1, int[][] matrix2) {
        int[][] result = new int[matrix1[0].length][matrix2.length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[0].length; j++) {
                for (int r = 0; r < matrix1.length; r++) {
                    result[i][j] += matrix1[i][r] * matrix2[r][j];
                }
            }
        }
        return result;
    }

    static int calculateDeterminant(int[][] matrix) {
        if (matrix.length != matrix[0].length) return 0;
        if (matrix.length == 1) return matrix[0][0];

        if (matrix.length == 2) return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0];

        if (matrix.length == 3) return matrix[0][0]*matrix[1][1]*matrix[2][2] +
                                       matrix[0][1]*matrix[1][2]*matrix[2][0] +
                                       matrix[0][2]*matrix[1][0]*matrix[2][1] -
                                       matrix[0][2]*matrix[1][1]*matrix[2][0] -
                                       matrix[0][0]*matrix[1][2]*matrix[2][1] -
                                       matrix[0][1]*matrix[1][0]*matrix[2][2];
        return matrix[0][0]*calculateDeterminant(crossOutRowAndColumn(matrix, 2 , 2));
    }

    static int[][] crossOutRowAndColumn(int[][] origMatrix, int i_index, int j_index) {
        int[][] result = new int[origMatrix.length - 1][origMatrix[0].length - 1];

        return result;
    }

    public static int[][] parseFromTxt(String txt_matrix) {
        String[] string_1d_matrix = txt_matrix.split("\n");
        String[][] string_2d_matrix = new String[string_1d_matrix.length][string_1d_matrix[0].split(" ").length];
        for (int i = 0; i < string_2d_matrix.length; i++) {
            string_2d_matrix[i] = string_1d_matrix[i].split(" ");
        }
        int[][] matrix = new int[string_2d_matrix.length][string_2d_matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = Integer.parseInt(string_2d_matrix[i][j]);
            }
        }
        return matrix;
    }

    public static void print(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.print('\n');
        }
    }

}