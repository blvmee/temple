package random.trashcan;

import java.util.Arrays;

public class ebe8 {

    public static void main(String[] args) {
        int cnt = 0;
        for(int x = 100026; x < 999927; x += 100) {
            boolean c1 = has3even(x);
            boolean c2 = has2odd(x);
            boolean c3 = hasRepetitiveDigits(x);
            if (c1 != c2 && !c3) {
                cnt++;
                System.out.println(x + " " + c1 + " " + c2);
            }
        }
        System.out.println(cnt);
    }

    static boolean has3even(int x) {
        int evenCunt = 0;
        while (x > 0) {
            if (x % 2 == 0) evenCunt++;
            x /= 10;
        }
        return evenCunt == 3;
    }

    static boolean has2odd(int x) {
        int oddCunt = 0;
        while (x > 0) {
            if (x % 2 == 1) oddCunt++;
            x /= 10;
        }
        return oddCunt == 2;
    }

    static boolean hasRepetitiveDigits(int x) {
        int[] digits = new int[6];
        for(int i = 5; i >= 0; i--) {
            digits[i] = x%10;
            x /= 10;
        }

        for (int i = 0; i < 5; i++) {
            for (int j = i + 1; j < 6; j++) {
                if (digits[i] != digits[j]) {
                    System.out.print(Arrays.toString(digits));
                    System.out.println(" false");
                } else {
                    return true;
                }
            }
        }
        return false;
    }

}
