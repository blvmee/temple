from functools import lru_cache

@lru_cache(None)
def fib(n):
    if n == 0:
        return 0
    if n <= 2:
        return 1
    return fib(n-1) + fib(n-2)

print(fib(50))

f = 0
prev = 0
curr = 1    
for i in range(int(input()) - 1):
    f = prev + curr
    prev = curr
    curr = f

print(f)