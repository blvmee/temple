package random.warfare;

import javax.swing.*;
import java.util.ArrayList;

public class Main {

    static ArrayList<Soldier> troops = new ArrayList<>();

    public static void main(String[] args) {
        Soldier.parseNames(100);
        warfare();
    }

    private static final int WIDTH = 600;
    static private final int HEIGHT = 600;

    public static int getWIDTH() {
        return WIDTH;
    }
    public static int getHEIGHT() {
        return HEIGHT;
    }

    static void warfare() {
        JFrame warfare = new JFrame("Warfare");
        warfare.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        warfare.setLocationRelativeTo(null);
        warfare.setSize(WIDTH,HEIGHT);
        warfare.setVisible(true);
        warfare.setResizable(true);
        Warfare game = new Warfare();
        warfare.add(game);
        generateTroops(game);
    }

    static void generateTroops(Warfare game) {
        troops.add(new Infantry(game));
        troops.add(new Infantry(game));
        troops.add(new Infantry(game));
        troops.add(new Infantry(game));
        troops.add(new Juggernaut(game));
        troops.add(new Juggernaut(game));
        troops.add(new Sniper(game));
        troops.add(new Samurai(game));
        showInfo();
    }

    static void showInfo() {
        for (Soldier i : troops) {
            System.out.println(i.toString());
        }
    }

}
