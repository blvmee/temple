package random.warfare;

import java.io.FileNotFoundException;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;
import java.util.Scanner;

import java.awt.Color;

public abstract class Soldier {

    Random randomizer = new Random();

    int posX = 0;
    int posY = 0;

    int health = 100;
    int armor = 50;
    int ammo;
    int ID;
    String weapon;
    String name;
    Color strategicalColor;

    public Soldier() {
        this.ID = 1000 + randomizer.nextInt(8999);
        this.name = getRandomName();
    }

    void fireGun(int clip) {
        for (int i = 0; (i < clip) && (this.ammo != 0); i++) {
            this.ammo--;
        }
        reload();
    }

    void reload() {
        System.out.println("gun reloaded");
    }

    abstract void attack();

    void takeDmg(int dmg) {
        if (dmg <= this.armor) {
            armor -= dmg;
        } else {
            dmg -= this.armor;
            this.health -= dmg;
            if (this.health <= 0) {
                die();
            }
        }
    }

    static ArrayList<String> names = new ArrayList<>();
    static int parsedNamesAmount = 0;

    static void parseNames(int quantity) {
        try {
            Scanner parser = new Scanner(new FileReader("src/com/company/random/warfare/names.txt"));
            for (int i = 0; i < 2943; i++) {
                names.add(parser.next());
            }
            Collections.shuffle(names);
            for (int i = 2942; i > quantity; i--) {
                names.remove(i);
            }
            parsedNamesAmount = quantity;
        } catch (FileNotFoundException e) {
            System.err.println("names.txt file not found");
        }
    }

    void die() {
        this.health = 0;
    }

    @Override
    public String toString() {
        return this.ID + " " + this.name + ", has " + this.health + " HP and " + this.armor + " armor, position: X = " + this.posX + "; Y = " + this.posY;
    }

    public String getRandomName() {
        try {
            return names.get(randomizer.nextInt(parsedNamesAmount));
        } catch (IllegalArgumentException exception) {
            return "Bob";
        }
    }

    void place(Warfare game) {
        this.posX = randomizer.nextInt(Main.getWIDTH());
        this.posY = randomizer.nextInt(Main.getHEIGHT());
        game.drawSoldier(posX, posY, this.strategicalColor);
    }

}
