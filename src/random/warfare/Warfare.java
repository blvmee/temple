package random.warfare;

import javax.swing.*;
import java.awt.*;

public class Warfare extends JPanel {

    public void paintComponent(Graphics g) {
        setLayout(null);
        super.paintComponent(g);
        this.setBackground(Color.GREEN);
    }

    void drawSoldier(int x, int y, Color color) {
        // getGraphics().setColor(color);
        getGraphics().drawLine(x,y,x + 5,y + 5);
    }

    Warfare getWarfare() {
        return this;
    }

}
