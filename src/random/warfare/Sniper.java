package random.warfare;

import java.awt.Color;

public class Sniper extends Soldier {

    public Sniper(Warfare game) {
        super();
        this.name = name + " [Sniper]";
        this.armor = 0;
        this.ammo = 50;
        this.weapon = "L96A1";
        this.strategicalColor = Color.LIGHT_GRAY;
        place(game);
    }

    void attack() {}
}
