package random.warfare;

import java.awt.Color;

public class Juggernaut extends Soldier {

    public Juggernaut(Warfare game) {
        super();
        this.name = name + " [Juggernaut]";
        this.ammo = 550;
        this.armor = 150;
        this.weapon = "M240";
        this.strategicalColor = Color.RED;
        place(game);
    }

    @Override
    void attack() {
        if (this.ammo != 0) {
            fireGun(75);
        }
    }

}
