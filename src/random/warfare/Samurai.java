package random.warfare;

import java.awt.Color;

public class Samurai extends Soldier {

    public Samurai(Warfare game) {
        super();
        this.name = this.name + " [Samurai]";
        this.weapon = "Katana";
        this.strategicalColor = Color.MAGENTA;
        place(game);
    }

    @Override
    void attack() {
        System.out.println("Swing swing!");
    }

}
