package random.warfare;

import java.awt.Color;

public class Infantry extends Soldier {

    public Infantry(Warfare game) {
        super();
        this.name = name + " [Infantry]";
        this.ammo = 150;
        this.weapon = "M16A3";
        this.strategicalColor = Color.BLACK;
        place(game);
    }

    @Override
    public void attack() {
        if (this.ammo != 0) {
            while (this.ammo != 0) {
                fireGun( 30);
                System.out.println(this.ammo);
            }
        } else {
            chargeDesperately();
        }
    }

    void chargeDesperately() {
        System.out.println("Za rossiyu!");
        die();
    }

}
