package random.flights;

public enum FlightStatus {

    YET_TO_TAKEOFF,
    IN_PROGRESS,
    COMPLETED,

}
