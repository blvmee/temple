package random.flights;

import java.util.Random;

public abstract class Plane {

    static Random randomize = new Random();

    private final String manufacturer;
    private final String model;
    private final int speed;
    private final int flightRange;
    private final int year;
    private final int cost;
    private final long serialNumber;
    private PlaneState state;
    private Airline airline;

    public int getSpeed() {
        return speed;
    }

    public Plane(String manufacturer, String model, int speed, int flightRange, int year, int cost) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.speed = speed;
        this.flightRange = flightRange;
        this.year = year;
        this.cost = cost;
        serialNumber = randomize.nextLong();
        state = PlaneState.FREE;
    }

    public Plane(String[] args) {
        this(args[0], args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]));
    }

    public int getCost() {
        return cost;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public long getSerialNumber() {
        return serialNumber;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline mother) {
        this.airline = mother;
    }

    public PlaneState getState() {
        return state;
    }

    public void setState(PlaneState state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return manufacturer + " " + model;
    }

    public String getMoreInfo() {
        return "S/N " + serialNumber + " " + model + " " + year + ", avg speed - " + speed + " km/h, max flight range - " + flightRange + " cost - $" + cost + ",000,000.";
    }

}
