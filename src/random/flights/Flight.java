package random.flights;

public class Flight {

    Route route;
    Plane plane;
    Passenger[] passengers;
    FlightStatus status;
    byte progress;
    DestinationCity from;
    DestinationCity to;
    // Time start;
    // Time finish;
    float x;
    float y;
    float deltaX;
    float deltaY;

    public Flight(Route route, Plane plane, Passenger[] passengers) {
        this.route = route;
        this.plane = plane;
        from = route.getFirstDestination();
        to = route.getLastDestination();
        progress = 0;
        updateStatus(FlightStatus.IN_PROGRESS);
        plane.setState(PlaneState.EN_ROUTE);

    }

    private void findDelta() {
        deltaX = Math.abs(to.getX() - from.getX()) / plane.getSpeed();
        deltaY = Math.abs(to.getY() - from.getY()) / plane.getSpeed();
    }

    public void makeMove() {
        x += deltaX;
        y += deltaY;
    }

    public void updateStatus(FlightStatus status) {
        this.status = status;
    }

    public String getProgress() {
        return progress % 101 + "%";
    }

    void checkIfIsFinished() {
        if ((Math.abs(x - to.getX()) < 1) && (Math.abs(y - to.getY()) < 1)) {
            progress = 100;
            status = FlightStatus.COMPLETED;
        }
    }

}
