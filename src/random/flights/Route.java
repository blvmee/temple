package random.flights;

import java.util.ArrayList;

public class Route {

    private ArrayList<DestinationCity> mainSequence = new ArrayList<>();
    private ArrayList<DestinationCity> capitalCitiesInTheRoute = new ArrayList<>();
    private DestinationCity firstDestination;
    private DestinationCity lastDestination;

    public Route(DestinationCity... destinations) {
        for (DestinationCity d : destinations) {
            mainSequence.add(d);
            if (d.isCapitalCity) {
                capitalCitiesInTheRoute.add(d);
            }
        }
        setFirstAndLastDestinations(destinations[0], destinations[destinations.length - 1]);
    }

    void setFirstAndLastDestinations(DestinationCity firstDestination, DestinationCity lastDestination) {
        this.firstDestination = firstDestination;
        this.lastDestination = lastDestination;
    }

    public ArrayList<DestinationCity> getSequence() {
        return mainSequence;
    }

    public int getSequenceLength() {
        return mainSequence.size();
    }

    public DestinationCity getFirstDestination() {
        return firstDestination;
    }

    public DestinationCity getLastDestination() {
        return lastDestination;
    }

}
