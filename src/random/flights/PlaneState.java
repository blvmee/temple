package random.flights;

public enum PlaneState {

    FREE,
    EN_ROUTE,
    BEING_REPAIRED,
    // MISSING,
    DESTROYED

}
