package random.flights;

public abstract class CrewMember {

    private final String firstName;
    private final String lastName;
    protected String job;
    private final int annualSalary;
    private final int code;

    public CrewMember(String firstName, String lastName, int annualSalary, int code) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.annualSalary = annualSalary;
        this.code = code;
    }

    @Override
    public String toString() {
        return "CrewMember{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", job='" + job + '\'' +
                ", code=" + code +
                '}';
    }
}
