package random.flights.dataNparsers;

import random.flights.CargoPlane;
import random.flights.PassengerPlane;
import random.flights.Plane;
import random.flights.PrivatePlane;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class PlaneParser {

    static ArrayList<String> failed = new ArrayList<>();

    public static ArrayList<Plane> parsePossiblePlanes() {
        ArrayList<Plane> possiblePlanes = new ArrayList<>();
        try {
            Scanner parser = new Scanner(new FileReader("src/com/company/random/flights/dataNparsers/Planes.flights_data"));
            while (parser.hasNextLine()) {
                String arg = parser.nextLine();
                try {
                    String[] args = arg.split("_");
                    switch (args[6]) {
                        case "Cargo":
                            possiblePlanes.add(new CargoPlane(args));
                            break;
                        case "Passenger":
                            possiblePlanes.add(new PassengerPlane(args));
                            break;
                        case "Private":
                            possiblePlanes.add(new PrivatePlane(args));
                            break;
                    }
                } catch (ArrayIndexOutOfBoundsException | NoSuchElementException | NumberFormatException e) {
                    if (!arg.equals("")) if (arg.charAt(0) != '~') failed.add(arg);
                    if (parser.hasNextLine()) parser.nextLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            possiblePlanes.add(new PassengerPlane("Airbus_A320_840_4500_1984_93_Passenger_160".split("_")));
        }
        return possiblePlanes;
    }

}
