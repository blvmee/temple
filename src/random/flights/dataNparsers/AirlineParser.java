package random.flights.dataNparsers;

import random.flights.Airline;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class AirlineParser {

    static ArrayList<String> failed = new ArrayList<>();

    public static ArrayList<Airline> parsePossibleAirlines() {
        ArrayList<Airline> possibleAirlines = new ArrayList<>();
        try {
            Scanner parser = new Scanner(new FileReader("src/com/company/random/flights/dataNparsers/Airlines.flights_data"));
            while (parser.hasNextLine()) {
                String arg = parser.nextLine();
                try {
                    possibleAirlines.add(new Airline(arg.split("_")));
                } catch (ArrayIndexOutOfBoundsException | NoSuchElementException | NumberFormatException e) {
                    if (!arg.equals("")) if (arg.charAt(0) != '~') failed.add(arg);
                    if (parser.hasNextLine()) parser.nextLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            possibleAirlines.add(new Airline("ЖМЫХ airlines"));
        }
        return possibleAirlines;
    }

}
