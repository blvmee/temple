package random.flights.dataNparsers;

import random.flights.DestinationCity;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class DestinationParser {

    static ArrayList<String> failed = new ArrayList<>();

    public static ArrayList<DestinationCity> parsePossibleDestinations() {
        ArrayList<DestinationCity> possibleDestinations = new ArrayList<>();
        try {
            Scanner parser = new Scanner(new FileReader("src/com/company/random/flights/dataNparsers/Cities.flights_data"));
            while (parser.hasNextLine()) {
                String arg = parser.nextLine();
                try {
                    possibleDestinations.add(new DestinationCity(arg.split(" ")));
                } catch (ArrayIndexOutOfBoundsException | NoSuchElementException | NumberFormatException e) {
                    if (!arg.equals("")) if (arg.charAt(0) != '~') failed.add(arg);
                    if (parser.hasNextLine()) parser.nextLine();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            possibleDestinations.add(new DestinationCity("Moscow Russia 13000000 0 0 true".split(" ")));
            possibleDestinations.add(new DestinationCity("Saint-Petersburg Russia 5000000 0 0 false".split(" ")));
        }
        // /*
        for (String s : failed) {
            System.out.println(s);
        }
        // */
        return possibleDestinations;
    }

}
