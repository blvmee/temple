package random.flights;

public class Hostess extends CrewMember {

    public Hostess(String firstName, String lastName, int annualSalary, int code) {
        super(firstName, lastName, annualSalary, code);
        this.job = "Hostess";
    }

}
