package random.flights;

public class PlaneDestroyer {

    void destroyPlane(Plane plane) {
        plane.setState(PlaneState.DESTROYED);
    }

}
