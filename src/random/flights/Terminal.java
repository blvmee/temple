package random.flights;

import random.flights.dataNparsers.PlaneParser;
import random.flights.storages.DestinationStorage;

import java.util.ArrayList;
import java.util.Random;

public class Terminal {

    /**
     * TODO 3D rendering irt, set fixed amount of ticks per second
     * TODO implement datetime & flights timetable
     * TODO expand passengers to plane crew, private plane owners, terrorists etc.
     * TODO ticket class (?)
     * TODO prevent planes colliding
     * TODO curvy route paths since now they're just straight lines from A to B when are visualised
     */

    static Random randomizer = new Random();

    public static void main(String[] args) {
        DestinationStorage storage = new DestinationStorage();
        storage.printSelf();
        ArrayList<Plane> list = PlaneParser.parsePossiblePlanes();
        for (Plane p : list) {
            System.out.println(p.toString());
        }
        while (true) {
            // one tick
            // makeMove();
            // updateMap();
        }
    }



}
