package random.flights;

public class DestinationCity {

    String name;
    String country;
    int population;
    private float x;
    private float y;
    boolean isCapitalCity;

    public DestinationCity(String name, String country, int population, float x, float y, boolean isCapitalCity) {
        this.name = name;
        this.country = country;
        this.population = population;
        this.x = x;
        this.y = y;
        this.isCapitalCity = isCapitalCity;
    }

    public DestinationCity(String[] args) {
        this(args[0], args[1], Integer.parseInt(args[2]), Float.parseFloat(args[3]), Float.parseFloat(args[4]), Boolean.parseBoolean(args[5]));
    }

    boolean isCapitalCity() {
        return isCapitalCity;
    }

    @Override
    public String toString() {
        return name +
                ", " + country +
                ", population=" + population +
                ", x=" + x +
                ", y=" + y +
                ", is capital=" + isCapitalCity;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
