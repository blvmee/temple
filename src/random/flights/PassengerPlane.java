package random.flights;

public class PassengerPlane extends Plane {

    int passengerSeats;
    int passengerSeatsAvailable;

    public PassengerPlane(String manufacturer, String model, int speed, int flightRange, int year, int cost, int passengerSeats) {
        super(manufacturer, model, speed, flightRange, year, cost);
        this.passengerSeats = passengerSeats;
        passengerSeatsAvailable = passengerSeats;
    }

    public PassengerPlane(String[] args) {
        super(args);
        passengerSeats = Integer.parseInt(args[7]);
        passengerSeatsAvailable = passengerSeats;
    }

    public String toString() {
        return super.toString() + ", " + (passengerSeats==passengerSeatsAvailable ? "all" : passengerSeatsAvailable == 0 ? "no" : passengerSeatsAvailable + "/" + passengerSeats) + " seats available.";
    }
}
