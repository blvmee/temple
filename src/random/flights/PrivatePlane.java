package random.flights;

public class PrivatePlane extends PassengerPlane {

    public PrivatePlane(String manufacturer, String model, int speed, int flightRange, int year, int cost, int passengerSeats) {
        super(manufacturer, model, speed, flightRange, year, cost, passengerSeats);
    }

    public PrivatePlane(String[] args) {
        super(args);
    }

}
