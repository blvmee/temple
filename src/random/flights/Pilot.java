package random.flights;

public class Pilot extends CrewMember {

    public Pilot(String firstName, String lastName, int annualSalary, int code) {
        super(firstName, lastName, annualSalary, code);
        this.job = "Pilot";
    }

}
