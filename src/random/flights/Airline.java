package random.flights;

import java.util.ArrayList;
import java.util.Random;

public class Airline {

    static Random randomizer = new Random();

    String name;
    ArrayList<Plane> fleet = new ArrayList<>();
    private int rating;
    private int budget;

    public Airline(String name) {
        this.name = name;
        rating = Math.abs(randomizer.nextInt() % 100);
        budget = randomizer.nextInt();
    }

    public Airline(String[] args) {
        this(args[0]);
    }

    public void buyNewAircraft(Plane plane) {
        if (plane.getCost() <= budget) {
            fleet.add(plane);
            budget -= plane.getCost();
        };
    }

    public int getRating() {
        return rating;
    }

    @Override
    public String toString() {
        return name +
                ", rating = " + rating +
                ", budget = " + budget;
    }

}
