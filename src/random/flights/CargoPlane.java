package random.flights;

public class CargoPlane extends Plane {

    private int cargoCapacity;
    private int cargoCapacityLeft;

    public CargoPlane(String manufacturer, String model, int speed, int flightRange, int year, int cost, int cargoCapacity) {
        super(manufacturer, model, speed, flightRange, year, cost);
        this.cargoCapacity = cargoCapacity;
        cargoCapacityLeft = cargoCapacity;
    }

    public CargoPlane(String[] args) {
        super(args);
        cargoCapacity = Integer.parseInt(args[7]);
        cargoCapacityLeft = cargoCapacity;
    }

    public String toString() {
        return super.toString() + ", available cargo space - " + cargoCapacityLeft + "/" + cargoCapacity + " tons.";
    }

    public void addCargo(int cargo) {
        if (cargo > cargoCapacityLeft) {
            System.out.println("Not enough space");
        } else {
            cargoCapacityLeft -= cargo;
            System.out.println("Successfully added " + cargo + " tons of cargo.");
        }
    }

}
