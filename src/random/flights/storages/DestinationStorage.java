package random.flights.storages;

import random.flights.DestinationCity;
import random.flights.dataNparsers.DestinationParser;

import java.util.Collections;
import java.util.List;

public class DestinationStorage {

    private List<DestinationCity> storage;

    public DestinationStorage() {
        storage = DestinationParser.parsePossibleDestinations();
    }

    public DestinationStorage(int size) {
        this();
        Collections.shuffle(storage);
        storage = storage.subList(0,size);
    }

    public List<DestinationCity> getStorage() {
        return storage;
    }

    public void printSelf() {
        for (DestinationCity destinationCity : storage) {
            System.out.println(destinationCity.toString());
        }
    }

}
