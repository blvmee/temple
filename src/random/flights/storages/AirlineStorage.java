package random.flights.storages;

import random.flights.Airline;
import random.flights.dataNparsers.AirlineParser;

import java.util.Collections;
import java.util.List;

public class AirlineStorage {

    private List<Airline> storage;

    public AirlineStorage() {
        storage = AirlineParser.parsePossibleAirlines();
    }

    public AirlineStorage(int size) {
        this();
        Collections.shuffle(storage);
        storage = storage.subList(0,size);
    }

    public List<Airline> getStorage() {
        return storage;
    }

    public void printSelf() {
        for (Airline airline : storage) {
            System.out.println(airline.toString());
        }
    }

}

