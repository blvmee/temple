package random.flights.storages;

import random.flights.Plane;
import random.flights.dataNparsers.PlaneParser;

import java.util.Collections;
import java.util.List;

public class PlaneStorage {

    private List<Plane> storage;

    public PlaneStorage() {
        this.storage = PlaneParser.parsePossiblePlanes();
    }

    public PlaneStorage(int size) {
        this();
        Collections.shuffle(storage);
        storage = storage.subList(0,size);
    }

    public List<Plane> getStorage() {
        return storage;
    }

    public void printSelf() {
        for (Plane plane : storage) {
            System.out.println(plane.toString());
        }
    }

}
