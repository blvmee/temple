package random.maze;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Window extends JPanel {

    Random rand = new Random();

    public void paintComponent(Graphics g) {
        setLayout(null);
        super.paintComponent(g);
        g.setColor(Color.YELLOW);
        this.setBackground(Color.CYAN);
        drawButton();
    }

    int i = rand.nextInt(Main.getWIDTH());
    int j = rand.nextInt(Main.getHEIGHT());

    void drawPatterns() {
        while ((i < Main.getWIDTH()) && (j < Main.getHEIGHT()) && (i > 0) && (j > 0)) {
            if (rand.nextBoolean()) {
                if (rand.nextBoolean()) {
                    i++;
                } else {
                    i--;
                }
            } else {
                if (rand.nextBoolean()) {
                    j++;
                } else {
                    j--;
                }
            }
            getGraphics().drawLine(i,j,i,j);
            System.out.println(i + " " + j);
        }
    }

    // int intFlag;
    // boolean flag = true;

    void drawButton() {
        JButton proceed = new JButton("proceed");
        proceed.setBounds(480, 200, 120, 40);
        proceed.addActionListener(e -> {
            drawPatterns();
            reset();
        });
        add(proceed);
        JButton stop = new JButton("stop");
        stop.setBounds(480, 260, 120, 40);
        stop.addActionListener(e -> {
            System.exit(8);
            // flag = intFlag % 2 != 0;
            // intFlag++;
            // stop.setText("start");
        });
        add(stop);
        JButton fiveTimes = new JButton("5 times");
        fiveTimes.setBounds(480,140,120,40);
        fiveTimes.addActionListener(e -> {
            for (int i = 0; i < 5; i++) {
                proceed.doClick();
            }
        });
        add(fiveTimes);
    }

    void reset() {
        i = rand.nextInt(640);
        j = rand.nextInt(480);
        // drawPatterns();
    }

}
