package random.javacorepro.generics;

public class Apple extends Fruit {
    public Apple() {
        this.weight = 1.0f;
    }
}
