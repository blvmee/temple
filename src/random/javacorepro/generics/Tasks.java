package random.javacorepro.generics;

import java.util.ArrayList;
import java.util.Arrays;

public class Tasks {
    public static void main(String[] args) {
    }

    public static <T> void swapTwoElementsInArray(T[] arr, int i, int j) {
        var t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }

    public static <T> ArrayList<T> arrToArrayList(T[] arr) {
        return new ArrayList<>(Arrays.asList(arr));
    }
}
