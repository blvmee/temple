package random.javacorepro.generics;

import java.util.ArrayList;
import java.util.List;

public class Box<E extends Fruit> {
    private float totalWeight;
    private List<E> content;

    public Box() {
        totalWeight = 0.0f;
        content = new ArrayList<>();
    }

    public void add(E e) {
        content.add(e);
        totalWeight += e.getWeight();
    }

    public void addAllFromOtherBox(Box<E> box) {
        content.addAll(box.getContent());
    }

    public float getTotalWeight() {
        return totalWeight;
    }

    public List<E> getContent() {
        return content;
    }

    public boolean compare(Box<E> o) {
        return this.getTotalWeight() == o.getTotalWeight();
    }
}
