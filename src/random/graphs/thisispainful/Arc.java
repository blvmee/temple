package random.graphs.thisispainful;

public class Arc {

    Node origin;
    Node end;

    int weight;
    int stream;

    public Arc(Node origin, Node end, int weight) {
        this.origin = origin;
        this.end = end;
        this.weight = weight;
        stream = 0;
    }

    public void incStream(int i) {
        if (stream + i <= weight) stream += i;
    }

}
