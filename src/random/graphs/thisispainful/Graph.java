package random.graphs.thisispainful;

import java.util.*;

public class Graph {


    private ArrayList<Node> nodes = new ArrayList<>();
    private ArrayList<Arc> arcs = new ArrayList<>();

    private Node source;
    private Node sink;

    public Graph() {
        setDefaultSource_N_Sink();
    }

    public Graph(ArrayList<Node> nodes, ArrayList<Arc> arcs) {
        super();
        this.nodes = nodes;
        this.arcs = arcs;
    }

    public Graph(int[][] matrix) {
        super();
    }

    static Graph copyOf(Graph graphToCopy) {
        return new Graph((ArrayList<Node>) List.copyOf(graphToCopy.getNodes()), (ArrayList<Arc>) List.copyOf(graphToCopy.getArcs()));
    }

    void setDefaultSource_N_Sink() {
        source = nodes.get(0);
        sink = nodes.get(nodes.size() - 1);
    }

    public Node getSource() {
        return source;
    }

    public Node getSink() {
        return sink;
    }

    public ArrayList<Node> getNodes() {
        return nodes;
    }

    public ArrayList<Arc> getArcs() {
        return arcs;
    }
}
