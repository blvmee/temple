package random.graphs.thisispainful;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class AlgorithmsImplementation implements Algorithms {

    @Override
    public int dijkstraAlgorithm(Graph g, Node from, Node to) {
        int shortestPath = 0;
        return shortestPath;
    }

    @Override
    public ArrayList<int[][]> floydAlgorithm(Graph g) {
        ArrayList<int[][]> result = new ArrayList<>();
        return result;
    }

    @Override
    public Graph fordFulkersonAlgorithm(Graph g) {
        Graph result = Graph.copyOf(g);
        Node focus = result.getSource();
        Node sink = result.getSink();;
        while (true) {
            // stage 1
            ArrayDeque<Arc> augmentingPath = new ArrayDeque<>();
            ArrayDeque<Node> nodeSequence = new ArrayDeque<>();
            focus.setMark(new Mark(false, Integer.MAX_VALUE));
            while (focus != sink) {
                Node[] availableNodes = focus.getAdjacentNodes();
                Arc bottleNeckArc = focus.getConnectedArc(availableNodes[0]);
                Node chosenNode = availableNodes[0];
                for (Node node : availableNodes) {
                    // choosing one with max capacity
                    if (!node.isMarked()) {
                        if (focus.getConnectedArc(node).weight > focus.getConnectedArc(chosenNode).weight) {
                            chosenNode = node;
                        }
                    }
                }
                Arc chosenArc = focus.getConnectedArc(chosenNode);
                if (chosenArc.weight < bottleNeckArc.weight) {
                    bottleNeckArc = chosenArc;
                }
                chosenNode.setMark(new Mark(chosenArc.end == chosenNode, bottleNeckArc.weight));
                focus = chosenNode;
                augmentingPath.add(chosenArc);


            }
            // stage 2
            while (focus != result.getSource()) {
            }
            result.getNodes().forEach(node -> node.setMark(null));


            break;
        }
        return result;
    }

    class Mark {

        public boolean negative_positive;
        public int varDelta;

        /**
         * Mark constructor
         *
         * @param negative_positive minus or plus
         * @param varDelta capacity
         */

        public Mark(boolean negative_positive, int varDelta) {
            this.negative_positive = negative_positive;
            this.varDelta = varDelta;
        }
    }

}
