package random.graphs.thisispainful;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Node {

    static int amountOfPoints;

    private final int id = amountOfPoints;
    private Set<Arc> incidentArcs = new HashSet<>();
    private List<Node> adjacentNodes = new ArrayList<>();

    // Ford-Fulkerson
    private AlgorithmsImplementation.Mark mark;

    public Node() {
        if (this instanceof DummyNode) return;
        amountOfPoints++;
    }

    public boolean connectToArc(Arc arc) {
        if (incidentArcs.add(arc)) {
            adjacentNodes.add(arc.origin == this ? arc.end : arc.origin);
            return true;
        }
        return false;
    }

    public Node[] getAdjacentNodes() {
        return (Node[]) adjacentNodes.toArray();
    }

    public Arc getConnectedArc(Node otherNode) {
        for (Arc arc : incidentArcs) {
            if (arc.origin == otherNode || arc.end == otherNode) {
                return arc;
            }
        }
        return new DummyArc();
    }

    public AlgorithmsImplementation.Mark getMark() {
        return mark;
    }

    public void setMark(AlgorithmsImplementation.Mark mark) {
        this.mark = mark;
    }

    public boolean isMarked() {
        return mark != null;
    }

}
