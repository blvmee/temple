package random.graphs.thisispainful;

import java.util.ArrayList;

public interface Algorithms {

    int dijkstraAlgorithm(Graph g, Node from, Node to);

    ArrayList<int[][]> floydAlgorithm(Graph g);

    Graph fordFulkersonAlgorithm(Graph g);

}
