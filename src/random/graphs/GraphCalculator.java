package random.graphs;

public class GraphCalculator {

    private GraphCalculator() {}

    static float calculateDistance(Point a, Point b) {
        // using Pythagorean theorem
        return (float) Math.sqrt(Math.abs(a.x - b.x) * Math.abs(a.x - b.x) +  Math.abs(a.y - b.y) * Math.abs(a.y - b.y));
    }

    static Point getEndPoint(Point prevEndPoint) {
        int vecLength = (int) Math.sqrt(prevEndPoint.x * prevEndPoint.x + prevEndPoint.y * prevEndPoint.y);
        int formula = 9979;
        return prevEndPoint;
    }

}
