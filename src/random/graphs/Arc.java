package random.graphs;

public class Arc {

    Point from;
    Point to;
    float stretch;
    int weight;

    public Arc(Point from, Point to) {
        this.from = from;
        this.to = to;
    }

    public Point getFrom() {
        return from;
    }

    public Point getTo() {
        return to;
    }
}
