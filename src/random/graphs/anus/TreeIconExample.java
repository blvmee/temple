package random.graphs.anus;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * represents an icon used in the directory tree; handles 'expanded' and
 * 'unexpanded' directories as well as indentation representing different
 * levels.
 * @author rcook
 *
 */
public class TreeIconExample
{
    public static int UNEXPANDED = 1;
    public static int EXPANDED = 2;

    private void say (String msg) { System.out.println(msg); }

    private static ImageIcon expandedIcon = null;
    private static ImageIcon unexpandedIcon = null;
    private static int       iconHeight = 0;
    private static int       iconWidth  = 0;

    private static ArrayList<ImageIcon> cachedExpandedIcons = new ArrayList<ImageIcon>();
    private static ArrayList<ImageIcon> cachedUnexpandedIcons = new ArrayList<ImageIcon>();

    static
    {
        expandedIcon = new ImageIcon(TreeIconExample.class.getResource("images/Expanded.GIF"));
        unexpandedIcon = new ImageIcon(TreeIconExample.class.getResource("images/Unexpanded.GIF"));
        iconHeight = unexpandedIcon.getIconHeight();
        iconWidth =  unexpandedIcon.getIconWidth();
    }

    public TreeIconExample()  {  }

    public static void main(String ... arguments)
    {
        JFrame frame = new JFrame("icon test");
        frame.setBackground(Color.blue);
        JLabel label = new JLabel("background test");
        label.setBackground(Color.magenta);
        TreeIconExample treeIcon = new TreeIconExample();
        ImageIcon icon = treeIcon.getIcon(2, false);
        label.setIcon(icon);
        frame.add(label);
        frame.pack();
        frame.setVisible(true);
    }

    public ImageIcon getIcon(int level, boolean expanded)
    {
        ImageIcon result = null;

        // generate this icon and store it in the cache before returning it.
        ImageIcon baseIcon = unexpandedIcon;
        if (expanded) { baseIcon = expandedIcon; }
        int iconH = iconHeight;
        int iconW = iconWidth*(level+1);

        BufferedImage bufferedImage = new BufferedImage(iconW,iconH,BufferedImage.TYPE_INT_ARGB);
        Graphics g = bufferedImage.getGraphics();

        g.fillRect(0, 0, iconW, iconH);
        g.drawImage(baseIcon.getImage(), iconWidth*level, 0, null);
        result = new ImageIcon(bufferedImage);

        return result;
    }
}