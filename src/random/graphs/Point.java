package random.graphs;

import java.util.ArrayList;

public class Point extends java.awt.Point {

    PointPanel pointPanel;
    private int id;

    public Point(java.awt.Point point) {
        this.x = point.x;
        this.y = point.y;
    }

    ArrayList<Arc> arcsFrom = new ArrayList<>();

    ArrayList<Arc> arcsTo = new ArrayList<>();

    public PointPanel getPointPanel() {
        return pointPanel;
    }

    public void setPointPanel(PointPanel pointPanel) {
        this.pointPanel = pointPanel;
    }

    void addArcTo(Arc arc) {
        arcsTo.add(arc);
    }

    void addArcFrom(Arc arc) {
        arcsFrom.add(arc);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
