package random.graphs;

public class Main {

    /**
     * Graphs v0.1
     * General controls:
     *      Space - change jumping mode
     *      Arrows/WASD - Jump to another point/arc
     *      Backspace - remove point/arc
     *      Enter - save as svg
     *      TBD
     * Point mode controls:
     *      Q - Create a point under a cursor
     *      Mouse (drag) - shift a dot in space
     *      R - toggle spider mode
     *      TBD
     * Arc mode controls:
     *      L - lock highlighted point
     *      Q - Connect locked point to highlighted point
     *      Tab - Alternate arc's orientation
     *      Shift (hold) - stretch an arc to a side
     *      I - split arc in two by putting a point in the middle TODO smooth split
     *      P - add parallel arc
     *      TBD
     */

    public static void main(String[] args) {
        Window window = new Window();
        new Thread(window).start();
    }

}
