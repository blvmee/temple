package random.graphs;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph {

    HashMap<Integer, Point> pointsByPointId = new HashMap<>();
    ArrayList<ArrayList<Arc>> arcsByPointId = new ArrayList<>();
    int pointsAmount = 0;

    void addPoint(Point newPoint) {
        newPoint.setId(pointsAmount);
        pointsByPointId.put(pointsAmount, newPoint);
        arcsByPointId.add(new ArrayList<>());
        pointsAmount++;
    }

    void addArc(Point from, Point to) {
        Arc addedArc = new Arc(from,to);
        arcsByPointId.get(from.getId()).add(addedArc);
        arcsByPointId.get(to.getId()).add(addedArc);
        pointsByPointId.get(from.getId()).addArcFrom(addedArc);
        pointsByPointId.get(to.getId()).addArcTo(addedArc);
    }

    ArrayList<Point> getAllPoints() {
        return pointsByPointId.isEmpty() ? new ArrayList<>() : new ArrayList<Point>(pointsByPointId.values());
    }

    Point getClosestPoint(Point relativePoint) {
        if (pointsByPointId.isEmpty()) return relativePoint;
        if (pointsByPointId.size() == 1) return pointsByPointId.get(0);
        Point closestPoint = null;
        float minDistance = Float.MAX_VALUE;
        ArrayList<Point> points = getAllPoints();
        for (Point p : points) {
            if (!p.equals(relativePoint)) {
                float distance = GraphCalculator.calculateDistance(relativePoint, p);
                if (distance < minDistance) {
                    minDistance = distance;
                    closestPoint = p;
                }
            }
        }
        return closestPoint;
    }

    boolean arcFound(Point from, Point to) {
        return false;
    }

    void removePoint(int id) {
        pointsByPointId.remove(id);
    }

}
