package random.graphs;

import javax.swing.*;

public class PointPanel extends JPanel {

    static JLabel blue = new JLabel(new ImageIcon(System.getProperty("user.dir") + "\\src\\point.png"));
    static JLabel red = new JLabel(new ImageIcon(System.getProperty("user.dir") + "\\src\\highlightedPoint.png"));
    Point point;
    boolean color;

    public PointPanel(Point point, boolean key) {
        this.point = point;
        setLayout(null);
        color = key;
        JLabel dot = key? red : blue;
        dot.setLocation(point);
        add(dot);
        setOpaque(false);
        setVisible(true);
        System.out.println("[panel] panel initialized");
    }

    public void switchColor() {
        add(color ? blue : red);
        repaint();
        System.out.println("[panel] color switched");
    }

    public boolean getColor() {
        return color;
    }

}
