package random.graphs.minigraphs;

import java.util.Arrays;
import java.util.LinkedList;

public class Edge implements Comparable<Edge> {

    public static final Edge BIG_DUMMY_EDGE = new Edge(Vertex.DUMMY_VERTEX, Vertex.DUMMY_VERTEX, 2147348647);

    private Vertex[] nodes;
    private int weight;

    public Edge(Vertex node1, Vertex node2, int weight) {
        this.nodes = new Vertex[]{node1, node2};
        this.weight = weight;
        node1.addEdge(this);
        node2.addEdge(this);
    }

    public Vertex getOppositeNode(Vertex vertex) {
        return nodes[0] == vertex ? nodes[1] : nodes[0];
    }

    public Vertex getFirstNode() {
        return nodes[0];
    }

    public Vertex getSecondNode() {
        return nodes[1];
    }

    public int getWeight() {
        return weight;
    }

    public boolean contains(Vertex vertex) {
        return getFirstNode() == vertex || getSecondNode() == vertex;
    }

    public int compareTo(Edge edge) {
        return Integer.compare(this.getWeight(), edge.getWeight());
    }

    @Override
    public String toString() {
        return "Edge{" +
                "nodes=" + Arrays.toString(nodes) +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Edge) return ((Edge) obj).getFirstNode() == getFirstNode()
                && ((Edge) obj).getSecondNode() == getSecondNode()
                && ((Edge) obj).getWeight() == getWeight();
        return super.equals(obj);
    }
}
