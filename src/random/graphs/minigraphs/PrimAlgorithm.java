package random.graphs.minigraphs;

import java.util.*;

public class PrimAlgorithm {

    private int minWeight;

    public PrimAlgorithm(Graph g) {
        minWeight = solve(g);
    }

    private int solve(Graph g) {
        int minWeight = 0;
        int n = g.getVertexCount();
        
        Set<Edge> chosenEdges = new HashSet<>();
        boolean[] visitedVertices = new boolean[n];

        int focus = (new Random()).nextInt(n);

        while (chosenEdges.size() != n - 1) {
            visitedVertices[focus] = true;
            Set<Edge> visibleEdges = new HashSet<>();
            for (int i = 0; i < visitedVertices.length; i++) {
                if (visitedVertices[i]) {
                    visibleEdges.addAll(g.getVertex(i).getEdges());
                }
            }
            visibleEdges.removeIf(edge -> chosenEdges.contains(edge) || (visitedVertices[edge.getFirstNode().getId()] & visitedVertices[edge.getSecondNode().getId()]));

            PriorityQueue<Edge> sortedVisibleEdges = new PriorityQueue<>(visibleEdges);

            Edge cheapestEdge;
            int closestNode = -1;
            // pick cheapest edge so that one if it's nodes was not visited previously
            do {
                cheapestEdge = sortedVisibleEdges.poll();
                int firstNode = cheapestEdge.getFirstNode().getId();
                int secondNode = cheapestEdge.getSecondNode().getId();
                if (visitedVertices[firstNode]) {
                    if (visitedVertices[secondNode]) continue;
                    closestNode = secondNode;
                } else closestNode = firstNode;
            } while (closestNode == -1);
            minWeight += cheapestEdge.getWeight();
            chosenEdges.add(cheapestEdge);
            focus = closestNode;
        }
        return minWeight;
    }

    public int getMinWeight() {
        return minWeight;
    }

}
