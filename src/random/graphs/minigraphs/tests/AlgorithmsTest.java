package random.graphs.minigraphs.tests;

import random.graphs.minigraphs.DijkstraAlgorithm;
import random.graphs.minigraphs.Digraph;

public class AlgorithmsTest {

    Digraph g;

    void createGraph() {

        g = new Digraph("" +
                "0, 0, 3, 0, 3, 0, 0, 11, \n" +
                "0, 0, 1, 0, 0, 0, 0, 0, \n" +
                "0, 0, 0, 0, 0, 5, 0, 0, \n" +
                "0, 0, 0, 0, 0, 0, 0, 0, \n" +
                "0, 0, 0, 0, 0, 0, 0, 5, \n" +
                "0, 42, 0, 0, 0, 0, 7, 0, \n" +
                "0, 0, 0, 3, 0, 0, 0, 0, \n" +
                "0, 0, 0, 0, 0, 0, 0, 0, ");


    }

    void test1() {
        int[][] results = new int[g.getVertexCount()][g.getVertexCount()];
        for (int i = 0; i < results.length; i++) {
            DijkstraAlgorithm d = new DijkstraAlgorithm((Digraph) g, i);
            results[i] = d.getResult();
        }
        int[][] actualResults = {
                {0 , 50, 3 , 18,  3,  8, 15,  8},
                {-1, 0 , 1 , 16, -1,  6, 13, -1},
                {-1, 47, 0 , 15, -1,  5, 12, -1},
                {-1, -1, -1, 0 , -1, -1, -1, -1},
                {-1, -1, -1, -1,  0, -1, -1,  5},
                {-1, 42, 43, 10, -1,  0,  7, -1},
                {-1, -1, -1, 3 , -1, -1,  0, -1},
                {-1, -1, -1, -1, -1, -1, -1,  0}
        };
        for (int i = 0; i < results.length; i++) {
        }
    }

}






