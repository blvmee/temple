package random.graphs.minigraphs;

public class ArcWithStream extends Arc implements Comparable<Arc> {

    private int stream;

    public ArcWithStream(Vertex origin, Vertex destination, int weight) {
        super(origin, destination, weight);
        stream = 0;
    }

    public ArcWithStream(Vertex origin, Vertex destination, int weight, int stream) {
        super(origin, destination, weight);
        this.stream = stream;
    }


    @Override
    public int compareTo(Arc arc) {
        if (arc instanceof ArcWithStream) {
            return Integer.compare(weight - stream, arc.getWeight() - ((ArcWithStream) arc).getStream());
        } else {
            return super.compareTo(arc);
        }
    }

    public boolean incStream(int increment) {
        if (stream + increment > weight) return false;
        stream += increment;
        return true;
    }

    public int getStream() {
        return stream;
    }

}
