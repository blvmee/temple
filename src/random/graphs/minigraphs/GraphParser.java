package random.graphs.minigraphs;

public class GraphParser {

    private GraphParser() {}

    /**
     * Creates a graph / digraph from a given text representation of an adjacency matrix
     *
     * @param txt_matrix String representing weight matrix / adjacency matrix of a new graph
     * @return Weighted graph / digraph
     */
    public static int[][] parseFromTxtAdjacencyMatrix(String txt_matrix) {
        String[] str1Dmatrix = txt_matrix.split("\n");
        String regex = txt_matrix.contains(", ") ? ", " : " ";
        String[][] str2D = new String[str1Dmatrix.length][str1Dmatrix[0].split(regex).length];
        for (int i = 0; i < str2D.length; i++) {
            str2D[i] = str1Dmatrix[i].split(regex);
        }
        int[][] adjMatrix = new int[str2D.length][str2D[0].length];
        int iDontSpeakEnglishSoIWontNameThisVariable = 0;
        for (int i = 0; i < adjMatrix.length; i++) {
            for (int j = 0; j < adjMatrix[0].length; j++) {
                adjMatrix[i][j] = Integer.parseInt(str2D[i][j]);
                if (i != j && adjMatrix[i][j] != adjMatrix[j][i]) iDontSpeakEnglishSoIWontNameThisVariable += 1; // TODO fix
            }
        }
        return adjMatrix;
    }

}
