package random.graphs.minigraphs;

import java.util.ArrayList;
import java.util.HashMap;

public class Digraph {

    private final HashMap<Integer, Vertex> vertices;
    private final ArrayList<Arc> arcs = new ArrayList<>();

    public Digraph() {
        vertices = new HashMap<>();
        vertices.put(0, new Vertex(0));
    }

    public Digraph(String matrix) {
        this(GraphParser.parseFromTxtAdjacencyMatrix(matrix));
    }

    public Digraph(int[][] adjacencyMatrix) {
        // create vertices
        int VertexCount = adjacencyMatrix.length;
        HashMap<Integer, Vertex> vertices = new HashMap<>();
        for (int i = 0; i < VertexCount; i++) {
            vertices.put(i, new Vertex(i));
        }
        // create arcs
        for (int i = 0; i < VertexCount; i++) {
            for (int j = 0; j < VertexCount; j++) {
                if (adjacencyMatrix[i][j] > 0) {
                    vertices.get(i).addAdjacentVertex(vertices.get(j));
                    vertices.get(j).addAdjacentVertex(vertices.get(i));
                    int arcWeight = adjacencyMatrix[i][j];
                    arcs.add(new Arc(vertices.get(i), vertices.get(j), arcWeight));
                }
            }
        }
        this.vertices = vertices;
    }

    public int[][] toAdjacencyMatrix() {
        int[][] matrix = new int[vertices.size()][vertices.size()];
        for (Arc arc : arcs) {
            matrix[arc.getOrigin().getId()][arc.getDestination().getId()] = arc.getWeight();
        }
        return matrix;
    }

    public int getVertexCount() {
        return vertices.size();
    }

    public int getArcCount() {
        return arcs.size();
    }

    public Vertex getVertex(Integer key) {
        return vertices.get(key);
    }

    static Digraph copyOf(Digraph g) {
        try {
            return new Digraph(g.toAdjacencyMatrix());
        } catch (Exception e) {
            return new Digraph(); // this should not ever happen
        }
    }

    public void removeVertex(int VertexId) {
        arcs.removeIf(arc -> arc.contains(vertices.get(VertexId)));
        vertices.remove(VertexId);
    }

    public void removeVertex(Vertex vertexToRemove) {
        removeVertex(vertexToRemove.getId());
    }

    public void removeArc(Arc arc) {
        arcs.remove(arc);
        for (Vertex vertex : vertices.values()) {
            
        }
    }

}
