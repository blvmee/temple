package random.graphs.minigraphs;

import java.util.ArrayList;
import java.util.HashMap;

public class Graph {

    private final HashMap<Integer, Vertex> nodes;
    private final ArrayList<Edge> edges = new ArrayList<>();

    public Graph() {
        nodes = new HashMap<>();
        nodes.put(0, new Vertex(0));
    }

    public Graph(String txtMatrix) {
        this(GraphParser.parseFromTxtAdjacencyMatrix(txtMatrix));
    }

    public Graph(int[][] adjacencyMatrix) {
        // create nodes
        int VertexCount = adjacencyMatrix.length;
        HashMap<Integer, Vertex> nodes = new HashMap<>();
        for (int i = 0; i < VertexCount; i++) {
            nodes.put(i, new Vertex(i));
        }
        // create arcs
        for (int i = 0; i < VertexCount; i++) {
            for (int j = i; j < VertexCount; j++) {
                if (adjacencyMatrix[i][j] > 0) {
                    nodes.get(i).addAdjacentVertex(nodes.get(j));
                    nodes.get(j).addAdjacentVertex(nodes.get(i));
                    int arcWeight = adjacencyMatrix[i][j];
                    edges.add(new Edge(nodes.get(i), nodes.get(j), arcWeight));
                }
            }
        }
        this.nodes = nodes;
    }

    static Graph copyOf(Graph g) {
        try {
            return new Graph(g.toAdjacencyMatrix());
        } catch (Exception e) {
            return new Graph();
        }
    }

    public int[][] toAdjacencyMatrix() {
        int[][] matrix = new int[nodes.size()][nodes.size()];
        for (Edge edge : edges) {
            matrix[edge.getFirstNode().getId()][edge.getSecondNode().getId()] = edge.getWeight();
        }
        return matrix;
    }
    public ArrayList<Edge> getEdges() {
        return edges;
    }

    public Edge[] getEdgesArray() {
        Edge[] result = new Edge[edges.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = edges.get(i);
        }
        return result;
    }

    public Vertex getVertex(int index) {
        return nodes.get(index);
    }

    public int getVertexCount() {
        return nodes.size();
    }

}
