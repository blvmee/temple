package random.graphs.minigraphs;

import java.util.*;

public class FordFulkersonAlgorithm {

    public FordFulkersonAlgorithm(Digraph g, int sourceId, int sinkId) {

    }

    private Set[] solve(Digraph g, int sourceId, int sinkId) {
        Map<Vertex, Mark> marks = new HashMap<>();

        for (int i = 0; i < g.getVertexCount(); i++) {
            marks.put(g.getVertex(i), null);
        }

        boolean nowhereToGo = false;

        while (!nowhereToGo) {
            Vertex focus = g.getVertex(sourceId);
            List<Arc> incidentArcs = new ArrayList<>();
            incidentArcs.addAll(focus.getArcsWithOriginInThisVertex());
            incidentArcs.addAll(focus.getArcsWithEndInThisVertex());
            while (focus != g.getVertex(sinkId)) {

            }
        }


        return null;
    }

}

class Mark {

    public boolean negative_positive;
    public int varDelta;

    /**
     * A Mark for execution of Ford-Fulkerson algorithm
     *
     * @param negative_positive false (-) if the vertex was reached against the arc orientation
     *                          true (+) if the vertex was reached in the correct direction
     * @param varDelta Value of the mark
     */

    public Mark(boolean negative_positive, int varDelta) {
        this.negative_positive = negative_positive;
        this.varDelta = varDelta;
    }

}