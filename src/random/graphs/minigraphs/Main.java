package random.graphs.minigraphs;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws Exception {
        String matrix = "" +
                "0 9 0 5 0 4 0 0\n" +
                "0 0 6 0 0 0 0 0\n" +
                "0 0 0 3 0 0 0 4\n" +
                "0 0 0 0 6 0 3 0\n" +
                "0 0 5 0 0 4 0 0\n" +
                "0 0 0 0 0 0 6 0\n" +
                "0 0 0 0 0 0 0 9\n" +
                "0 0 0 0 0 0 0 0";
        Digraph digraph = new Digraph(matrix);
        DijkstraAlgorithm d = new DijkstraAlgorithm(digraph,0);
        matrix = "" +
                "0, 5, 7, 0, 0, 0, 0, 11, \n" +
                "5, 0, 11, 0, 0, 0, 3, 0, \n" +
                "7, 11, 0, 7, 7, 0, 7, 0, \n" +
                "0, 0, 7, 0, 0, 0, 7, 0, \n" +
                "0, 0, 7, 0, 0, 10, 0, 20, \n" +
                "0, 0, 0, 0, 10, 0, 5, 0, \n" +
                "0, 3, 7, 7, 0, 5, 0, 0, \n" +
                "11, 0, 0, 0, 20, 0, 0, 0, ";
        matrix = "" +
                "0, 5, 7, 0, 0, 0, 0, 11, 0, 0, 0, 3, 0, 0, \n" +
                "5, 0, 11, 0, 0, 0, 3, 0, 0, 7, 0, 0, 0, 0, \n" +
                "7, 11, 0, 7, 7, 0, 7, 0, 0, 0, 0, 0, 0, 0, \n" +
                "0, 0, 7, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, \n" +
                "0, 0, 7, 0, 0, 10, 0, 20, 7, 0, 0, 0, 0, 0, \n" +
                "0, 0, 0, 0, 10, 0, 5, 0, 0, 0, 0, 0, 0, 0, \n" +
                "0, 3, 7, 7, 0, 5, 0, 0, 0, 0, 0, 0, 0, 15, \n" +
                "11, 0, 0, 0, 20, 0, 0, 0, 0, 0, 0, 0, 3, 0, \n" +
                "0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, \n" +
                "0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, \n" +
                "0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, \n" +
                "3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, \n" +
                "0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, \n" +
                "0, 0, 0, 0, 0, 0, 15, 0, 0, 11, 10, 0, 0, 0, ";
        Graph g2 = new Graph(matrix);
        PrimAlgorithm p = new PrimAlgorithm(g2);
        System.out.println(p.getMinWeight());
        KruskalAlgorithm k = new KruskalAlgorithm(g2);
        System.out.println(k.getMinWeight());
    }

    void a() {
        Digraph g = new Digraph("" +
                "0 1 1 0 0 0 0 0 0 0 \n" +
                "1 0 1 1 0 0 0 0 0 0 \n" +
                "1 1 0 0 0 0 0 0 1 0 \n" +
                "0 1 0 0 1 0 1 0 0 1 \n" +
                "0 0 0 1 0 1 0 0 0 0 \n" +
                "0 0 0 0 1 0 1 0 0 0 \n" +
                "0 0 0 1 0 1 0 1 0 0 \n" +
                "0 0 0 0 0 0 1 0 0 0 \n" +
                "0 0 1 0 0 0 0 0 0 1 \n" +
                "0 0 0 1 0 0 0 0 1 0");
    }

    private static void soutAdjMatrix(Digraph g) {
        int[][] gm = g.toAdjacencyMatrix();
        for (int i = 0; i < gm.length; i++) {
            for (int j = 0; j < gm.length; j++) {
                System.out.print(gm[i][j] + " ");
            }
            System.out.println();
        }
    }

}
