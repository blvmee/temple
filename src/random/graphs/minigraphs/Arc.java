package random.graphs.minigraphs;

public class Arc implements Comparable<Arc> {

    public static final Arc BIG_DUMMY_ARC = new Arc(Vertex.DUMMY_VERTEX, Vertex.DUMMY_VERTEX, 2147348647);

    protected Vertex origin;
    protected Vertex destination;

    protected int weight;

    public Arc(Vertex origin, Vertex destination, int weight) {
        this.origin = origin;
        this.destination = destination;
        this.origin.addStartingArc(this);
        this.destination.addEndingArc(this);
        this.weight = weight;
    }

    public boolean contains(Vertex vertex) {
        return origin == vertex || destination == vertex;
    }

    public Vertex getOrigin() {
        return origin;
    }

    public Vertex getDestination() {
        return destination;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int compareTo(Arc arc) {
        return Integer.compare(getWeight(), arc.getWeight());
    }

}
