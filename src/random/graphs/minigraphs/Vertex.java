package random.graphs.minigraphs;

import java.util.*;
import java.util.stream.Collectors;

public class Vertex {

    public static final Vertex DUMMY_VERTEX = new Vertex(-1);

    private final int id;

    public Vertex(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    // FOR NON ORIENTED GRAPHS

    Set<Edge> edges = new HashSet<>();
    private final Map<Integer, Vertex> adjacentNodesById = new HashMap<>();

    public void addEdge(Edge edge) {
        if (!edges.add(edge)) return;
        Vertex nodeToAdd = edge.getFirstNode() == this ? edge.getSecondNode() : edge.getFirstNode();
        adjacentNodesById.putIfAbsent(nodeToAdd.getId(), nodeToAdd);
    }

    public void addAdjacentNode(Vertex node) {
        adjacentNodesById.putIfAbsent(node.getId(), node);
    }

    public ArrayList<Edge> getEdges() {
        return new ArrayList<>(edges);
    }


    /**
     *
     *                        For digraphs
     *
     */

    Set<Arc> arcs = new HashSet<>();
    private final ArrayList<Arc> arcsWithOriginInThisVertex = new ArrayList<>();

    private final ArrayList<Arc> arcsWithEndInThisVertex = new ArrayList<>();

    private final Map<Integer, Vertex> adjacentVerticesById = new HashMap<>();

    public void addStartingArc(Arc arc) {
        if (!arcs.add(arc)) return;
        arcsWithOriginInThisVertex.add(arc);
        Vertex vertexToAdd = arc.getDestination();
        adjacentVerticesById.putIfAbsent(vertexToAdd.getId(), vertexToAdd);
    }

    public void addEndingArc(Arc arc) {
        if (!arcs.add(arc)) return;
        arcsWithEndInThisVertex.add(arc);
        Vertex vertexToAdd = arc.getOrigin();
        adjacentVerticesById.putIfAbsent(vertexToAdd.getId(), vertexToAdd);
    }

    public List<Arc> getArcsWithOriginInThisVertex() {
        return arcsWithOriginInThisVertex;
    }

    public List<Arc> getArcsWithEndInThisVertex() {
        return arcsWithEndInThisVertex;
    }

    public Map<Integer, Vertex> getAdjacentVertices() {
        return adjacentVerticesById;
    }

    public void addAdjacentVertex(Vertex vertex) {
        adjacentVerticesById.putIfAbsent(vertex.getId(), vertex);
    }

    public boolean contains(Arc arc) { // but why?
        for (Arc a : arcsWithEndInThisVertex) {
            if (arc == a) return true;
        }
        for (Arc a : arcsWithOriginInThisVertex) {
            if (arc == a) return true;
        }
        return false;
    }

    public boolean removeArc(Arc arc) {
        if (arcsWithOriginInThisVertex.remove(arc)) {
            arc.getDestination().removeArc(arc);
            return true;
        } else if (arcsWithEndInThisVertex.remove(arc)) {
            arc.getOrigin().removeArc(arc);
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "id=" + id +
                '}';
    }
}
