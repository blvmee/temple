package random.graphs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;


public class Window extends JFrame implements Runnable, KeyListener, MouseMotionListener {

    private final int WIDTH = 1920;
    private final int HEIGHT = 1080;

    Graph graph;

    Point highlightedPoint;
    Point lockedPoint;

    boolean spiderMode = false; // to be explained here

    public Window() {
        graph = new Graph();
        this.setSize(WIDTH, HEIGHT);
        this.setLocationRelativeTo(null);
        this.setBackground(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        addMouseMotionListener(this);
        addKeyListener(this);
        JTextArea controls = new JTextArea("Space - change mode\n" +
                "Q - Place point/arc\n" +
                "L - Lock highlighted point\n" +
                "R - enable/disable spider mode");
        controls.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 10));
        controls.setLineWrap(true);
        controls.setWrapStyleWord(true);
        controls.setVisible(true);
        JPanel ctrls = new JPanel();
        ctrls.add(controls);
        ctrls.setLocation(0, 0);
        ctrls.setVisible(true);
        add(ctrls);
    }

    @Override
    public void run() {

    }

    ImageIcon blueDot = new ImageIcon(System.getProperty("user.dir") + "\\src\\blueDot.png");
    ImageIcon redDot = new ImageIcon(System.getProperty("user.dir") + "\\src\\redDot.png");
    ImageIcon greenDot = new ImageIcon(System.getProperty("user.dir") + "\\src\\greenDot.png");

    @Override
    public void paint(Graphics g) {
        ArrayList<Point> pointsToDisplay = graph.getAllPoints();
        for (Point p : pointsToDisplay) {
            ImageIcon dot = blueDot;
            if (p.equals(highlightedPoint)) {
                if (!highlightedPointIsLocked) {
                    dot = redDot;
                } else {
                    if (p.equals(lockedPoint)) {
                        dot = redDot;
                    } else {
                        dot = greenDot;
                    }
                }
            } else {
                if (highlightedPointIsLocked && p.equals(lockedPoint)) {
                    dot = redDot;
                }
            }
            g.drawImage(dot.getImage(), p.x-15, p.y-15, (new Color(255, 255, 255, 20)), null);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case 'Q':
            case 'q':
                place();
                break;
            case 0x20: // alt
                altMode();
                break;
            case 'W':
            case 0x26:
                // up
                break;
            case 'S':
            case 0x28:
                // down
                break;
            case 'D':
            case 0x25:
                // left
                break;
            case 'A':
            case 0x27:
                // right
                break;
            case 8: // Backspace
                removeComponent();
                break;
            case 'R':
                toggleSpiderMode();
                break;
            case 0x10: // Shift
                // toggle shift mode
                break;
            case 'L':
            case 'l':
                lockHighlightedPoint();
                break;
            case '\t':
                //invertArc();
                break;
            case 'I':
                // split arc
                break;
            case 'P':
                // parallel arc
                break;
            default:
                System.out.println("wrong key pressed");
                break;
        }
        repaint();
    }

    private void wasd(String key) {

    }

    boolean highlightedPointIsLocked;

    private void lockHighlightedPoint() {
        if (GraphCalculator.calculateDistance(new Point(MouseInfo.getPointerInfo().getLocation()), highlightedPoint) <= 50) {
            lockedPoint = highlightedPoint;
        } else {
            lockedPoint = highlightedPoint;
            highlightedPointIsLocked = !highlightedPointIsLocked;
        }
    }

    private void toggleSpiderMode() {
        spiderMode = !spiderMode;
        System.out.println(spiderMode ? "you are a spider" : "you are not a spider");
    }

    private boolean pointMode_arcMode;

    private void altMode() {
        pointMode_arcMode = !pointMode_arcMode;
        System.out.println("changed mode");
    }

    private void place() {
        if (!pointMode_arcMode) {
            placeDot();
        } else {
            if (lockedPoint != null) {
                placeArc(lockedPoint, highlightedPoint);
            } else {
                System.out.println("pls lock a point");
            }
        }
    }

    private void placeDot() {
        System.out.println(MouseInfo.getPointerInfo().getLocation().toString());
        Point p = new Point(MouseInfo.getPointerInfo().getLocation());
        graph.addPoint(p);
        System.out.println("new point placed at " + p.toString());
        if (spiderMode) {
            if (highlightedPoint != null) {
                placeArc(highlightedPoint, p);
            }
            if (!highlightedPointIsLocked) highlightedPoint = p;
        }
    }

    private void placeArc(Point from, Point to) {
        // getGraphics().drawLine(from.x, from.y, to.x, to.y);
        Point endPoint = GraphCalculator.getEndPoint(to);
        drawArrowLine(getGraphics(),from.x, from.y, endPoint.x, endPoint.y,30,10);
    }


    private void removeComponent() {
        if (!pointMode_arcMode) {
            graph.removePoint(highlightedPoint.getId());
            highlightedPoint = graph.getClosestPoint(new Point(MouseInfo.getPointerInfo().getLocation()));
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        Point mousePos = new Point(e.getPoint());
        Point closestPoint = graph.getClosestPoint(mousePos);
        highlightedPoint = closestPoint;
        // getGraphics().drawImage(blueDot.getImage(), e.getX(), e.getY(), null);
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    /**
     * Draw an arrow line between two points.
     * @param g the graphics component.
     * @param x1 x-position of first point.
     * @param y1 y-position of first point.
     * @param x2 x-position of second point.
     * @param y2 y-position of second point.
     * @param d  the width of the arrow.
     * @param h  the height of the arrow.
     */
    private void drawArrowLine(Graphics g, int x1, int y1, int x2, int y2, int d, int h) {
        int dx = x2 - x1, dy = y2 - y1;
        double D = Math.sqrt(dx*dx + dy*dy);
        double xm = D - d, xn = xm, ym = h, yn = -h, x;
        double sin = dy / D, cos = dx / D;

        x = xm*cos - ym*sin + x1;
        ym = xm*sin + ym*cos + y1;
        xm = x;

        x = xn*cos - yn*sin + x1;
        yn = xn*sin + yn*cos + y1;
        xn = x;

        int[] xpoints = {x2, (int) xm, (int) xn};
        int[] ypoints = {y2, (int) ym, (int) yn};

        g.drawLine(x1, y1, x2, y2);
        g.fillPolygon(xpoints, ypoints, 3);
    }

}
